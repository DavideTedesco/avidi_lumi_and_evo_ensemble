#!/bin/sh

#  ALEVO-gitpush.sh
#
# use it by terminal typing:
# bash ALEVO-gitpush.sh

cd /Users/davide/gitlab/DavideTedesco/avidi_lumi_and_evo_ensemble

git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
