#!/usr/bin/env bash

# BN-Tedesco-gitpush-linux.sh
#
# use it by terminal typing:
# bash BN-Tedesco-gitpush-linux.sh

cd /home/davide/gitlab/DavideTedesco/avidi_lumi_and_evo_ensemble
git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push
