primorigo = \new Staff \with{
         
             instrumentName = "So."
        shortInstrumentName = "So."
                midiInstrument = "choir aahs"
\consists "Page_turn_engraver" 

        \new Voice{
        }
    }    {
      \primavoce}
 
    
secondorigo =    \new Staff \with{
        instrumentName = "Ms."
        shortInstrumentName = "Ms."
                midiInstrument = "choir aahs"
                \consists "Page_turn_engraver" 

}{\secondavoce}
      
    
 terzorigo =  \new Staff \with{
        instrumentName = "Te."
  shortInstrumentName = "Te."
          midiInstrument = "choir aahs"
\consists "Page_turn_engraver" 
   \clef "treble_8"
 }{\terzavoce}
    
quartorigo =   \new Staff \with{
        instrumentName = "Te. B."
  shortInstrumentName = "Te. B."
        midiInstrument = "choir aahs"
\consists "Page_turn_engraver" 
   \clef "treble_8"
}{\quartavoce}
    
quintorigo =    \new Staff \with{
        instrumentName = "Br."
        shortInstrumentName = "Br."
        midiInstrument = "choir aahs"
        \consists "Page_turn_engraver" 
    \clef bass
}{\quintavoce} 