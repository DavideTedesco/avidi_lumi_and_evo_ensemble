primorigo = \new Staff \with{
         
             instrumentName = "Soprano"
        shortInstrumentName = "So."
                midiInstrument = "choir aahs"
\consists "Page_turn_engraver" 

        \new Voice{}
    }    {\primavoce}
 
    
secondorigo =    \new Staff \with{
        instrumentName = "Mezzosoprano"
        shortInstrumentName = "Ms."
                midiInstrument = "choir aahs"
                \consists "Page_turn_engraver" 

}{\secondavoce}
      
    
 terzorigo =  \new Staff \with{
        instrumentName = "Tenore "
  shortInstrumentName = "Te."
          midiInstrument = "choir aahs"
\consists "Page_turn_engraver" 
   \clef "treble_8"
 }{\terzavoce}
    
quartorigo =   \new Staff \with{
        instrumentName = "Tenore Beatbox"
  shortInstrumentName = "Te. B."
        midiInstrument = "choir aahs"
\consists "Page_turn_engraver" 
   \clef "treble_8"
}{\quartavoce}
    
quintorigo =    \new Staff \with{
        instrumentName = "Baritono"
        shortInstrumentName = "Br."
        midiInstrument = "choir aahs"
        \consists "Page_turn_engraver" 
    \clef bass
}{\quintavoce} 