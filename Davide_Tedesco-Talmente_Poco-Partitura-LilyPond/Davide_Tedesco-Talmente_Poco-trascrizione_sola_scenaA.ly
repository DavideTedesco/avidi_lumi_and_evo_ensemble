%[1]   VERSIONE ==============
\version "2.20.0"

%[2]  BLOCCO PAPER==================================
\include "template/papera4P.ly"

%[3]  BLOCCO HEADER==================================
 \header { 
                 %title = "Talmente Poco (scena A)"
                 title = "Talmente Poco"
                 composer = "Davide Tedesco"
                 subsubtitle = "per due voci femminili e tre voci maschili"
                 %tagline = "2022"
                 %dedication = "dedicato ad EVO Ensemble ed Avidi Lumi"

 }

%[4]  BLOCCO NOTAZIONE==================================

\include "personalizzazioni.ly"

\include "voci/sopranoA.ly"
\include "voci/mezzosopranoA.ly"
\include "voci/tenoreA.ly"
\include "voci/tenorebeatboxA.ly"
\include "voci/baritonoA.ly"
%\include "voci/tempo.ly"


%[5]  BLOCCO IMPOSTAZIONE RIGHI==========================
\include "template/Ensemble5Voci_scena_A.ly"


%[6]  BLOCCO SCORE==================================
       \score {

%[6a]  parti da includere========================

 \new ChoirStaff <<
       %\rigozero
	\primorigo
	\secondorigo
	\terzorigo
	\quartorigo
	\quintorigo
	
	>>

%[6b] PERSONALIZZAZIONE GRAFICA==========
 \layout {
            %indent = -1

    \context {
            %\Staff \RemoveEmptyStaves
      %\override VerticalAxisGroup.remove-first = ##t
      \Score
       \accidentalStyle choral-cautionary
      \override DynamicText.direction = #UP
      %per spostare le indicazioni dinamiche sopra la partitura
      \override DynamicLineSpanner.direction = #UP
      \override Flag.stencil = #modern-straight-flag
      
      \override Glissando.thickness = #10/3
      % \hide SpanBar 
      %++++++++tweaks for propotional notation
      proportionalNotationDuration = #(ly:make-moment 1/8)
      \override SpacingSpanner.uniform-stretching = ##t
      \override Score.SpacingSpanner.strict-note-spacing = ##t

      %\override PaperColumn.used = ##t
      %++++++++tweaks for hiding bars, bar numbers and time
      %\hide Staff.BarLine
      %\hide TimeSignature
      \omit BarNumber    
            \override TimeSignature.font-size = #3

      

    }
    %\context {
     % \Voice
     % \override HorizontalBracket.direction = #UP
      %\override HorizontalBracket.bracket-flare = #'(0 . 0)
      %\override HorizontalBracket.edge-height  = #'(1 . 1)
      %\override HorizontalBracket.shorten-pair =#'(0 . -2.5)
      %\consists "Horizontal_bracket_engraver"
      

    %}
  }
%[6c] OUTPUT MIDI===================
} 

%[7] OPTIONS FOR EXPORT===================
%disable hyperlinks
%\pointAndClickOff