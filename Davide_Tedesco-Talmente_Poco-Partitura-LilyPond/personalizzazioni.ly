hideTime = \hide Staff.TimeSignature
timeVis = \time 4/4

%===================RESPIRO

respiro = \markup{
  \italic {Sulle pause far sentire il respiro}
}
%===================UP AND DOWN ARROW
updownarrow = \markup {
        \left-align
      \hspace #4

  \fontsize #-4
  \override #'(thickness . 1)
  \overlay {
    \draw-line #'(0 . 2)
    \arrow-head #Y #DOWN ##f
    \translate #'(0 . 2)\arrow-head #Y #UP ##f

  }
}

%===================ESPIRAZIONE

espirare = \markup {

               \left-align
                 \hspace #4
                \override #'(thickness . 2)
                  \overlay {

        \draw-line #'(6 . 0)

          \translate #'(6 . 0)\arrow-head #X #RIGHT ##f

                  }

  }
  
  %===================INSPIRAZIONE

inspirare = \markup {
               \left-align
                 \hspace #4
                \override #'(thickness . 2)
                  \overlay {

        \draw-line #'(6 . 0)

          \translate #'(0 . 0)\arrow-head #X #LEFT ##f

                  }

  }
  
  %================SPOSTAMENTO INSPIRAZIONE ED ESPIRAZIONE
posRespiroC = \once \override TextScript.extra-offset = #'(0 . 1.2)                                                                        

posRespiro = \once \override TextScript.extra-offset = #'(0 . 1)                                                                        


%===========RALLENTANDO
rallentando = \markup { \italic {rall.}
} 



alniente = \once \override Hairpin.circled-tip = ##t

%rvoiceless = \markup {\column{\line{r}\line{\draw-circle #0.1 #0.1 ##f}}}
rvoiceless = \markup {r\sub{\draw-circle #0.1 #0.05 ##f}}
%===================TEMPO
%https://toolstud.io/music/bpm.php
quaranta = \tempo 4 = 40
cinquanta = \tempo 4 = 50
sessanta = \tempo 4 = 60
settanta = \tempo 4 = 70
ottanta = \tempo 4 = 80

%===================INDICAZIONI SULLE SCENE

scenaF = \mark \markup { \italic
  
    All'ingresso della propria parte posizionarsi nella propria posizione della scena F
  
}    

markPosition = \override Score.RehearsalMark.self-alignment-X = #left 
%===================SCENE
A = \mark \markup {\box A  \column{\italic {\line{Le luci si accendono,}\line{ gli esecutori sono già in sala con gli occhi chiusi. \epsfile #X #6 #"epsScene/A-figure0.eps"}}}}
scenaA = \mark \markup {\epsfile #X #6 #"epsScene/A-figure0.eps"}
Bzero = \mark \markup {\box B0} 
scenaBone = \mark \markup {\epsfile #X #6 #"epsScene/B1-figure0.eps"}
scenaBtwo = \mark \markup {\epsfile #X #6 #"epsScene/B2-figure0.eps"}
scenaBthree = \mark \markup {\epsfile #X #6 #"epsScene/B3-figure0.eps"}
scenaBfour = \mark \markup {\epsfile #X #6 #"epsScene/B4-figure0.eps"}
Bone = \mark \markup { \italic{raggiunge con velocemente} \box B1} 
Btwo = \mark \markup { \column{\line{\italic{raggiunge} \box B2} \line{\italic{velocemente}} \line{\italic{sul punto}} \line{\italic{di domanda}}}} 
Bthree= \mark \markup { \column{\line{\italic{raggiunge} \box B3}\line{\italic{camminando verso}} \line{\italic{ destra e sinistra}}}} 
Bfour = \mark \markup { \italic{\column{\line{raggiunge molto} \line{lentamente ed esitando}}} \box B4} 
C = \mark \markup {\box C} 
scenaC = \mark \markup {\epsfile #X #6 #"epsScene/C-figure0.eps"}
D = \mark \markup {\box D} 
scenaDone = \mark \markup {\epsfile #X #6 #"epsScene/D1-figure0.eps"}
scenaDtwo = \mark \markup {\epsfile #X #6 #"epsScene/D2-figure0.eps"}
scenaDthree = \mark \markup {\epsfile #X #6 #"epsScene/D3-figure0.eps"}
scenaDfour = \mark \markup {\epsfile #X #6 #"epsScene/D4-figure0.eps"}
scenaDfive = \mark \markup {\epsfile #X #6 #"epsScene/D5-figure0.eps"}

E = \mark \markup {\box E \epsfile #X #6 #"epsScene/E-figure0.eps"} 
F = \mark \markup { \center-align
                                {\box F  }
                                
                                {\italic {Tutti: posizionarsi a semicerchio.}}
                                \epsfile #X #6 #"epsScene/F-figure0.eps"
                              }

%Posizionarsi a semicerchio.
 %===================NOTA CERCHIO

cerchio   = {
\once \override NoteHead.stencil = #ly:text-interface::print
\once \override NoteHead #'font-size = #-5

\once \override NoteHead.text = #(markup #:musicglyph "accidentals.sharp" ) }


piuf = \markup {più \dynamic{mf}}


%====================Lunghezza eventi

%
 %===================Elementi Scena A



hideNH =  \hide NoteHead
hideStem =  \hide Stem
hideFlag =  \hide Flag

uhideNH =  \undo \hide NoteHead
uhideStem =  \undo \hide Stem

icsNH   = \once {
\once \override NoteHead.stencil = #ly:text-interface::print
\override NoteHead #'font-size = #1
    \override Stem.Y-offset = #-0.25

\once \override NoteHead.text = #(markup #:musicglyph "noteheads.s2cross" ) }

%===================Indicazioni sui secondi

diecisec  = \once\override HorizontalBracketText.text = "~10\"" 
ottosec  = \once\override HorizontalBracketText.text = "~8\"" 
cinquesec  = \once\override HorizontalBracketText.text = "~5\""
quasec  = \once\override HorizontalBracketText.text = "~4\""
tresec  = \once\override HorizontalBracketText.text = "~3\""
duesec  = \once\override HorizontalBracketText.text = "~2\""
%===================Specifiche per le HorizontalBracket da un secondo
%esempio d'uso
 %\posParUnSec
%                                        \xNote b4^\parentesiUnSec^\unsec^\markup{\dynamic{mf}} 
unsec  = \markup{"~1\""}
posParUnSec = \once \override TextScript.extra-offset = #'( 0 . -0.566 )
parentesiUnSec = \markup{
\combine
\translate #'(0 . -1.4)\draw-line #'(0 . 1)   						
\combine					
\translate #'(0 . -0.4)\draw-line #'(3.9 . 0) 
\translate #'(3.9 . -1.4)\draw-line #'(0 . 1)   							
}

colorBracket =
#(define-music-function (y-lower y-upper color) 
     (number? number? color?)
    #{
      \once\override HorizontalBracket.stencil =
        $(lambda (grob)
          (let* (
            (area (ly:horizontal-bracket::print grob))
              (X-ext (ly:stencil-extent area X))
              (Y-ext (ly:stencil-extent area Y)))
            (set! Y-ext (cons y-lower y-upper))
            (ly:grob-set-property! grob 'layer -10)
            (ly:make-stencil (list 'color color
              (ly:stencil-expr (ly:round-filled-box X-ext Y-ext 0))
              X-ext Y-ext))))
      \once\override HorizontalBracket.Y-offset = #0
      \once\override HorizontalBracket.shorten-pair = #'(0 . -2.4)
    #})
    
pausaPos =\override Stem.Y-offset = #1.5
pausaLung = \override Stem.length = #5

revStemY =\revert Stem.Y-offset
revStemLung = \revert	 Stem.length

AltOn =
#(define-music-function (mag) (number?)
  #{ \override Stem.length = #(* 7.0 mag)
      \override NoteHead.font-size =
#(inexact->exact (* (/ 6.0 (log 2.0)) (log mag))) #})

AltOff = {
  \revert Stem.length
  \revert NoteHead.font-size
}


play = \startGroup

stop = \stopGroup


%=============scena B

sussurrando = \markup {\italic{sussurrando}}
interrogativo = \markup {\italic{interrogativo}}
csub = \markup {\italic{crescendo sub.}}
inspiro = \markup {\italic{inspiro}}
espiro = \markup {\italic{espiro}}
occhi = \markup {\italic{chiude gli occhi dopo aver raggiunto la scena \box{C}}}

%====NOTA PARALLELOGRAMMA (da Paxxx)

parallelogram =
  #(ly:make-stencil (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      0 0.25 moveto
      1.3125 0.75 lineto
      1.3125 -0.25 lineto
      0 -0.75 lineto
      closepath
      fill
      grestore" )
    (cons 0 1.3125)
    (cons -.75 .75))
    
quadro = {
\once\override NoteHead #'no-ledgers = ##t
  \once\override Accidental #'stencil = ##f
  \once\override NoteHead.stencil = \parallelogram   } 
  
 %====NOTA RIGA PIENA
 
 rettangolo =
  #(ly:make-stencil (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      0 0.35 moveto
      3.95 0.35 lineto
      3.95 -0.35 lineto
      0 -0.35 lineto
      closepath
      fill
      grestore" )
    (cons 0 1.3125)
    (cons -.75 .75))
    
pieno = {
%\once
\override NoteHead #'no-ledgers = ##t
  %\once
  \override Accidental #'stencil = ##f
  %\once
  \override NoteHead.stencil = \rettangolo   } 
  
 %======DAL RUMORE ALL'INTONAZIONE
 noiseToSound = \markup{\italic{dal rumore al suono}}
 
 %======XNOTE CIRCLE
 xCircleOn = {
 \override NoteHead.stencil = #ly:text-interface::print
  \override NoteHead.text = #(markup #:musicglyph "noteheads.s2xcircle" ) 
}

xCircleOnce = {
 \once \override NoteHead.stencil = #ly:text-interface::print
 \once \override NoteHead.text = #(markup #:musicglyph "noteheads.s2xcircle" ) 
}

xCircleOff = {
  \revert NoteHead.stencil
}


xNoteOn = {
 \override NoteHead.stencil = #ly:text-interface::print
  \override NoteHead.text = #(markup #:musicglyph "noteheads.s2cross" ) 
}

xNoteOnce = {
 %\once \override NoteHead.stencil = #ly:text-interface::print
  \once \override NoteHead.text = #(markup #:musicglyph "noteheads.s2cross" ) 
}

xNoteOff = {
    \revert NoteHead.stencil
}

 
 whistleOn = {
  \override NoteHead.stem-attachment = #'(0 . -1.5)
  \override NoteHead.stencil =
    #(lambda (grob)
       (make-circle-stencil 0.6 0.2
         (> (ly:grob-property grob 'duration-log) 1)))
  \override Staff.AccidentalPlacement.right-padding = #0.6
}

whistleOnce = {
   \override NoteHead.stencil = #ly:text-interface::print
  \once \override NoteHead.stem-attachment = #'(0 . -1.5)
  \once \override NoteHead.stencil =
    #(lambda (grob)
       (make-circle-stencil 0.6 0.2
         (> (ly:grob-property grob 'duration-log) 1)))
  \once \override Staff.AccidentalPlacement.right-padding = #0.6
}
whistleOff = {
  \revert NoteHead.stem-attachment
  \revert NoteHead.stencil
  \revert Staff.AccidentalPlacement.right-padding
}

%==================GESTI E MOVIMENTI
%mani
mani = \markup { \epsfile #X #5 #"hands/mani.eps"} 
strofinare = \markup { \epsfile #X #3 #"hands/strofinare.eps"} 
pugno = \markup { \epsfile #X #3 #"hands/pugno.eps"} 
mano_sinistra = \markup { \epsfile #X #2.6 #"hands/mano_sinistra.eps"} 
mano_destra = \markup { \epsfile #X #2.6 #"hands/mano_destra.eps"} 
orario = \markup { \epsfile #X #4 #"hands/orario_mano.eps"} 
antiorario = \markup { \epsfile #X #4 #"hands/antiorario_mano.eps"}
%corpo
biagia = \markup { \epsfile #X #2.5 #"hands/braccia_e_gambe_aperte.eps"} 
bia = \markup { \epsfile #X #2.5 #"hands/mani_in_alto.eps"} 
fpa = \markup { \epsfile #X #2.5 #"hands/finto_passo_avanti.eps"} 
enunciando = \markup { \epsfile #X #3 #"hands/enunciando.eps"} 
bsc = \markup { \epsfile #X #2 #"hands/braccia_semichiuse.eps"} 
%indicazioni per le mani sulle cosce
su = \markup\combine\override #'(thickness . 1.3) \draw-line #'(0 . 2)\raise #2 \arrow-head #Y #UP ##f
giu = \markup\combine\arrow-head #Y #DOWN ##f \override #'(thickness . 1.3) \draw-line #'(0 . 2)