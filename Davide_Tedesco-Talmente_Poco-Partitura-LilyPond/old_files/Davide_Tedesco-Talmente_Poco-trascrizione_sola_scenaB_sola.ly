\version "2.20.0"

\header { 
                 title = "Talmente Poco-scena B"
                 composer = "Davide Tedesco"
                 subsubtitle = "for The EVO Ensemble"
                 tagline = "2022"
 }

\include "personalizzazioni.ly"

sussurrando = \markup {\italic{sussurrando}}
interrogativo = \markup {\italic{interrogativo}}
csub = \markup {\italic{crescendo sub.}}
inspiro = \markup {\italic{inspiro}}
espiro = \markup {\italic{espiro}}
occhi = \markup {chiude gli occhi}

tacet = 
#(define-music-function (arg str) (number? string?)
   #{
      \compressEmptyMeasures
      \once\override MultiMeasureRest.expand-limit = #1
      \once\override MultiMeasureRest.minimum-length = #arg 
      \once\override MultiMeasureRestNumber.font-name = "Vera Bold"
      \once\override MultiMeasureRestNumber.stencil =
        #(lambda (grob)(grob-interpret-markup grob 
           (markup #:whiteout #:pad-markup .8 str)))
      \once\override MultiMeasureRest.layer = #-2
      \once\override MultiMeasureRestNumber.layer = #-1
      \once\override MultiMeasureRestNumber.Y-offset = #-.5 
   #})


\score{

  \relative{ \cinquanta \Bzero \alniente d'2\mp\>  bes' r8^\respiro g4.\mp fis2 r8 e'2\pp 
                   dis4. b1 c2 r8 \Bone f!4\mp\>\glissando   fis,2.\pp  r8   a4->  (b2)  r4 
                   \xNote b4^\sussurrando \xNote b4 \xNote b4 \xNote b4^\interrogativo
                   r8 g\pp\< (bes) c1\glissando-> d4\f c \breathe fis->
                   bes, (bes) r4 \alniente fis2\mf\>\glissando g
                  \tuplet 3/4 {\xNote b4\f \xNote b\mf \xNote b\p}
                  a2.\p (gis16) r8 a2^\csub r4
                  d2 \tweak style #'zigzag \glissando b r8 \xNote  b4^inspiro r8 \xNote b4^espiro
                  r8 g4\ppp (fis2.)^\occhi

  }\addlyrics{ba- sta ba- sta sta tal- men- te ta a 
              ba-sta ba- sta ba- sta?
              qu- u- a- nto ba sta? ba ___ [s] [s] [s]
              a a e ___  o o o}
}


 \layout {
    \context {
      \Score
      \override DynamicText.direction = #UP
      %per spostare le indicazioni dinamiche sopra la partitura
      \override DynamicLineSpanner.direction = #UP
      \override Flag.stencil = #modern-straight-flag
      
      \override Glissando.thickness = #10/3
      % \hide SpanBar 
      %==========tweaks for propotional notation
      proportionalNotationDuration = #(ly:make-moment 1/16)
      \override SpacingSpanner.uniform-stretching = ##t
      %\remove "Separating_line_group_engraver"
      %\override Score.SpacingSpanner.strict-note-spacing = ##t
      %\override PaperColumn.used = ##t
      %\override SpacingSpanner.strict-grace-spacing = ##t
      %\override Beam.breakable = ##t
      %\override Glissando.breakable = ##t
      %\override TextSpanner.breakable = ##t
      %==========tweaks for hiding bars, bar numbers and time
      %\hide BarLine
      \hide TimeSignature
      \omit BarNumber

    }
  }
