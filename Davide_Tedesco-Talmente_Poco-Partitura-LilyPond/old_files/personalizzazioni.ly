hideTime = \hide Staff.TimeSignature
tempoVis = \tempo 4 = 40
timeVis = \time 4/4

 %===================NOTA CERCHIO

cerchio   = {
\once \override NoteHead.stencil = #ly:text-interface::print
\once \override NoteHead #'font-size = #1

\once \override NoteHead.text = #(markup #:musicglyph "accidentals.sharp" ) }
