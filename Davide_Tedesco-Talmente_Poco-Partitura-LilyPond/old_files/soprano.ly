\relative {\mark \default  \bar "!" \timeVis \tempoVis \cerchio   dis''2\f dis2 (dis1\p\> dis1)  \fermata 
                          \breathe  ais1\< (ais1f ais1)
                          \breathe  d1\glissando ( cis1^\updarrow) 
                          \breathe ais1 \glissando(a?2^\updarrow) r2 
                          \breathe d1 \glissando (e^\updarrow)
                          \breathe  fis,\glissando(g^\updarrow) 
                          \breathe e'1(\mp\< (e\f)   \bar  "|."} 

                           \addlyrics {ba sta \(o a e o u\) \(o\)}