\version "2.19.83"

%\include "note.ly"
\include "papera4L.ly"
%\include "italiano.ly"
\include "personalizzazioni.ly"

\header { 
  title = "Talmente Poco"
  composer = "Davide Tedesco"
  %subsubtitle = "for The EVO Ensemble"
  tagline = "2022"
}

EtoF = \markup {
  \rounded-box \line {
    E  →    F 
  }
}       

F =\markup {
  \rounded-box \line {
    F 
  }
}       

sceneF = \markup {
  
    All'ingresso della propria parte posizionarsi nella propria posizione della scena F
  
}    

%\override TextScript.extra-offset = #'(-4 . 0)
ciotto = c8_\markup{
 \general-align #Y #DOWN {
    %\with-dimensions #'(0 . 0) #'(0 . 0)
    %\epsfile #Y #3 #"home/davide/gitlab/DavideTedesco/avidi_lumi_and_evo_ensemble/img/suonoforma01-1b.eps"
  }
}

%ear = \markup{  \epsfile #X #20 #"img/pippo.png"}
%ear = \includegraphics{ #X #20 #"pippo.png"}
nulla = \markup{o}

%pippo =     c1^\markup { \tiny { \char U+1F442 } }

%%%%%%%%A

%%%%%%%%B

%%%%%%%%C


%%%%%%%%D

%%%%%%%%E


%%%%%%%%F

updarrow = \markup{↕}


sopranoNotesE =  \relative {c} \addlyrics {a}
sopranoNotesF =\relative {\mark \default  \bar "!" \timeVis \tempoVis \cerchio   dis''2\f dis2 (dis1\p\> dis1)  \fermata 
                          \breathe  ais1\< (ais1f ais1)
                          \breathe  d1\glissando ( cis1^\updarrow) 
                          \breathe ais1 \glissando(a?2^\updarrow) r2 
                          \breathe d1 \glissando (e^\updarrow)
                          \breathe  fis,\glissando(g^\updarrow) 
                          \breathe e'1(\mp\< (e\f)   \bar  "|."
                          <<{a4 b4 a4 } \\ {\cerchio c \cerchio c c}>>}
                           %\addlyrics {ba sta \(o a e o u\) \(o\)}
sopranoNotes =   \sopranoNotesF \sceneF %\EtoF \F     

mezzoSopranoNotesE = \relative {c} \addlyrics {a}
mezzoSopranoNotesF = \relative  {\bar "!"r1 gis'1\p\> (gis1)\fermata  ais'1\mp\< (ais\f)  \bar  "|."}\addlyrics {ta}
                                                                                                                 %\addlyrics{ a e o o u}\addlyrics{ o}
mezzoSopranoNotes = { \mezzoSopranoNotesF}

tenoreNotesE = \relative {c} \addlyrics {a}
tenoreNotesF = \relative {\bar "!"r1 r2 b'2\p\> (b1)\fermata  f'1\mp\< (f\f)  \bar  "|."}\addlyrics {me o}
tenoreNotes = { \tenoreNotesF}

tenoreBeatboxNotesE = \relative {c} \addlyrics {a}

tenoreBeatboxNotesF = \relative {\bar "!"r1 r1 cis''\p\>\fermata dis,1\mp\< (dis\f) \bar  "|."}\addlyrics {te o}
tenoreBeatboxNotes = { \tenoreBeatboxNotesF}


baritonoNotesE = \relative {c} \addlyrics {a}
baritonoNotesF = \relative{\bar "!"r1 r1 r4 dis4\p\> dis2\fermata c1\mp\< (c\f) \bar  "|."}\addlyrics {po co}
baritonoNotes ={  \baritonoNotesF}



\score {
  
  \new ChoirStaff <<

  
    \new Staff \with{
      
        %\hide BarLine
        instrumentName = "Veronica"
        shortInstrumentName = "Ve."
        \new Voice{}
    } \sopranoNotes   
 
    
    \new Staff \with{
        %\hide BarLine
        instrumentName = "Virginia"
        shortInstrumentName = "Vi."
      \new Voice {}
    } \mezzoSopranoNotes 
      
    
    \new Staff \with{
        %\hide BarLine
        instrumentName = "Alessandro "
  shortInstrumentName = "A."
      \new Voice {}
    }\tenoreNotes
    
    \new Staff \with{
        %\hide BarLine
        instrumentName = "Erwin"
  shortInstrumentName = "Er."
      \new Voice {}
    }\tenoreBeatboxNotes
    
    \new Staff \with{
        %\hide BarLine
        instrumentName = "Emanuele "
        shortInstrumentName = "Em."
    \clef bass
      \new Voice {}
    }\baritonoNotes 
    
    
  >>
  
  \layout {
    \context {
      \Score
      \override DynamicText.direction = #UP
      %per spostare le indicazioni dinamiche sopra la partitura
      \override DynamicLineSpanner.direction = #UP
      \override Flag.stencil = #modern-straight-flag			
      % \hide SpanBar 
      proportionalNotationDuration = #(ly:make-moment 1/16)
    }
  }
}