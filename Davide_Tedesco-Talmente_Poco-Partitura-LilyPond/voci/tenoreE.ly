

terzavoce =
\relative c'{
  r1 dis\glissando\p c\glissando e\glissando b (b) r1 r1 e,\mf (f) f4\f\> (g2\glissando) b1 (b4) (b2\pp) r4 e,4 (dis2) d! cis1 r1
  f\f (f) (f) r1 f (f2) r2
  r1 g1 (g\glissando)_\markup{[i]-----------[o]} a
  r1 r2 r4  c4 (c1) r1 r1 r2  b2\f (b1)\fermata r4
  f4\p e4 d4  e2\mf f1 
}                                     
                                   \addlyrics {[o] [o] [o] [o]  po__-__co li- ittle-e pe - tit
                                   Tal__-__ment__-__te ba__-__sta li ___ [i] po da- vve- ro ba- sta}