
quartavoce =
\relative{
    \override Staff.NoteHead.style = #'triangle

%==============C
\stopStaff
  \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  
                        r1 r4 r1 r4 r1 r4 r2  \mark \markup{rall...}   b4^\mani^\markup{\italic{apre gli occhi}}_\giu b4_\su b4_\giu
                        \tuplet 6/5 {b4_\giu b4_\su b4_\giu b4_\su b4_\giu   b4_\su}
                        r1 r4 r2. r2. r2.
                        r2 b4^\mano_sinistra_\giu
                        b2.^\orario
                        b4^\mano_sinistra_\su r2
                        r4 b2^\orario
                        r2 b8^\pugno r8
                        b2^\orario
                        r2 r2
                        r4 b4^\mani_\giu
                        (b4_\su b4_\giu)
                        b2^\bia
                        b2^\bia
                        b2.^\bia
                        r2.
                        b2.^\fpa
                        b2.^\fpa
                        r2.
                        r1
                        b1^\bia
                        r1
                        r2. b4^\pugno
                        r1
                        r1 r4 r2
                        b4^\fpa r2
                        b4^\mani_\giu b4_\su b4_\giu b4_\su b4_\giu
                        b4^\pugno r4
                        b2^\biagia
                        b2^\bia
                        r1
                        r1
                        b1^\strofinare
                        b2.
                        b4^\pugno^\markup{
                          \column{
                          \line{\italic{come se il fischio}} 
                          \line{\italic{nascesse dal colpo}
                        }}}
    \override Staff.NoteHead.style = #'default
     \stopStaff
  \revert Staff.StaffSymbol.line-count    
  \startStaff
             \undo \hide Staff.Clef	
              \clef "treble"
 
                      \alniente \whistleOnce b'1^\markup{da \dynamic{pp} subito \dynamic{f}}
  }
