
primavoce =  \relative{
                                    
%==============F    

                                      \time 6/2
                                     %\once \override TextScript.extra-offset = #'(2 . 3)
                                     \once \undo \hide Score.BarLine \bar "!"
                                     \F \sessanta \markPosition  % \bar  "!"
                                    dis''2\f dis!2~\alniente dis1~\p\> \noBreak  dis2~dis \fermata 
                                      dis1\p\< (dis) (dis\f )
                                      \breathe                                     
                                      d!1\f\> (d) (d\pp )
                                    \breathe
                                    a!1\mp\<\glissando (
                                    d\glissando 
                                    \once \override TextScript.extra-offset = #'(-1 . 4.5)                                              
                                    d\f_\updownarrow )
                                    r1
                                    \breathe
                                    a1\mf\glissando (
                                    \once \override TextScript.extra-offset = #'(-1 . 3.5)                                              
                                    b_\updownarrow)
                                    r1
                                    \breathe
                                    r4
                                    b4\mf\glissando (
                                     \once \override TextScript.extra-offset = #'(-1 . 4)                                              
                                    d2_\updownarrow )
                                    \breathe
                                     c2\p\<\glissando (
                                     \once \override TextScript.extra-offset = #'(-1 . 5.5)                                             
                                     f_\updownarrow)
                                    \breathe  
                                    \alniente d1\f\>  (\noBreak   d ) \mark \rallentando
                                    \breathe
                                      \alniente fis2\f\>\glissando  (
                                      fih\glissando)(
                                     \once \override TextScript.extra-offset = #'(-1 . 5.5)                                              
                                    fis_\updownarrow  ) r2
                                    \breathe
                                    g\glissando
                                     (\once \override TextScript.extra-offset = #'(-1 . 6)                                              
                                     gih_\updownarrow)
                                    r1
                                    \breathe
                                     e1\mp\<\mark \markup{\italic{\column{ \line{chiudendo gli occhi}\line{dopo la fine del suono}}}}
                                      (e\ff)\fermata
                                    %\undo \hide Score.BarLine
                                    r1
                                    \bar  "|." }
                                    %<<{a4 b4 a4 } \\ {\cerchio c \cerchio c c}>>}%}
%==============TESTO                                     
                                   \addlyrics {ba sta [a] [a] [e] [a] [o] [u] [a] [o] [e] [o]}