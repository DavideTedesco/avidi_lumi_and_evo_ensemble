rigozero = \relative c''{  \stopStaff 
                          \omit Staff.Clef 
                         
\override Staff.StaffSymbol.line-positions = #'( 0.4 0.2 0 -0.2 -0.4 ) 
         %\override TextScript.extra-offset = #'(-1 . 0)                                                                        

   \startStaff
 s2^\markup{\bold 0''}  s2\bar"!" 
 s2^\markup{\bold 10''}  s2\bar"!" 
 s2^\markup{\bold 20''}  s2\bar"!" 
 s2^\markup{\bold 30''} s2\bar"!"  
 s4^\markup{\bold 35''} s4\bar"!"  
 s4^\markup{\bold 40''} s4\bar"!"  
 s4^\markup{\bold 45''}  s4\bar"!"  
 s4^\markup{\bold 50''}  s4\bar"!"  
 s4^\markup{\bold 55''}  s4\bar"!"  
 s4^\markup{\bold 60''}  s4\bar"!"  
 s4^\markup{\bold 65''}  s4\bar"!"  
 s4^\markup{\bold 70'}s4 \hide BarLine

  }  