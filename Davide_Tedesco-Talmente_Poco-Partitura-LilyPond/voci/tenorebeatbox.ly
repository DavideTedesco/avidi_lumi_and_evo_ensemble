quartavoce = \relative {                     
%==============F   
                        \bar "!"r1 r1 \alniente cis'\p\>\fermata 
                        dis,1\mp\<\glissando (
                        c! \glissando
                        \once \override TextScript.extra-offset = #'(-1 . 3)
                        cis_\updownarrow\f)
                        \breathe                                     
                        dis1\mp\<\glissando (
                        c \glissando
                        \once \override TextScript.extra-offset = #'(-1 . 3)
                        d!_\updownarrow\f)
                        \breathe
                         fis\mp\<\glissando (
                         c \glissando
                       \once \override TextScript.extra-offset = #'(-1 . 3)                                                  
                         c\f_\updownarrow)
                        r1
                        \breathe
                        fis\mf\glissando (
                        \once \override TextScript.extra-offset = #'(-1 . 3)                                              
                        d_\updownarrow)
                        r1
                        \breathe
                        g2\pp\< (g\mf)
                         \breathe
                         c2\p\< (c\ff)
                        \breathe
                        r2 r4  f,\mp (f1)
                        \breathe
                        r2 \alniente e\p\>\glissando eih1
                        \breathe
                         f2. \glissando(
                         \once \override TextScript.extra-offset = #'(-1 . 3)                                                                       
                         fis_\updownarrow) 
                        r2
                        \breathe                                 
                        e1\mp\< (e\ff)\fermata
                        r1
%==============TESTO                            
                        \bar  "|."}\addlyrics {te [a] [e] [e] [a] [o] [o] [i] [a] [e] [e] [o] }