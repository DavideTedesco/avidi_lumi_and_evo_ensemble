
 %mostraBarlineTratt = \once \undo \hide Score.BarLine \bar "!"

primavoce =     
\relative c''{
%==============C
%\override TextScript.outside-staff-priority = #1
  \hide Staff.Clef

\stopStaff
  \override Staff.StaffSymbol.line-count = #1

  \startStaff 
  %\colorBracket #0 #1 #grey
  %\diecisec
  
                                     \D  %\startGroup  \scenaA
                                     
                                \xNote c     
}