

terzavoce =
\relative{
    \override Staff.NoteHead.style = #'triangle

%==============C
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \hide Staff.Clef
  \startStaff
  
  r1 r4 r1 r4 r1 r4 r1 r4
  \tuplet 6/5 {b4^\mani^\markup{\italic{apre gli occhi}}_\giu b4_\su b4_\giu b4_\su b4_\giu   b4_\su}
  r1 r4
  r2. r2. 
  \tuplet 4/3 {b4^\mani_\giu b4_\su b4_\giu b4_\su}
  r4 b4^\mano_sinistra_\giu r4
  
  r2.
  r4 r4 
  \once \override TextScript.extra-offset = #'(-2 . 0)
  b4^\mani_\giu
  \once \override TextScript.extra-offset = #'(0 . -4)
  b2^\orario r4
  b4^\mani_\giu b4_\su r4
  b2^\orario
  r2
  r2
  b8^\pugno r8 r4
  b2^\bia
  r2
  b2^\bia
  b2.^\bia
  r2.
  r2.
  b2.^\fpa
  r2.
  r1
  b1^\bia
  r1
  r2. b4^\pugno
  r1
  r1 r4
  r2 b2.^\bia
  r1 r4
  b4^\pugno r4
  b2^\biagia
  b2^\bia
  r1
  b1^\enunciando
  r1
  r1
  r1^\markup{
    \column{
    \line{\italic{si sposta per}}
    \line{\italic{raggiungere} \box{E}}
    
  }}
}
