
secondavoce =     
  \relative{ 
    \time 4/4
    
    \cinquanta \alniente d'2\mp\>  bes' r8^\respiro g4.\mp fis2 r8 e'2\pp 
                   dis4.\mf b1 \Bone c2\pp r4   f!4\mp\glissando \scenaBone   fis,2.^\markup{subito \dynamic{pp}}  r8   a8->  (b2)  r4 
                   \xNote b4^\sussurrando  \xNote b4 \xNote b4 \xNote b4^\interrogativo
                   r8 g16\pp\< (bes) c1\glissando-> d4\f  \Btwo c  \breathe fis->   \scenaBtwo 
                   bes, ( bes) r4 \alniente fis2\mf\>  \glissando 
                   \once \hide DynamicText
                   g\p r2 \break
                  \tuplet 3/4 {\xNote b4\f \xNote b\mf \xNote b\p}
                  a2.\p   (gis16) r8. \Bthree a2^\csub   
                  r4 \scenaBthree d4\f \tweak style #'zigzag \glissando 
                  b2.   \xNote  b4_\inspirare
                  
                   bes2\glissando\mf g16 r8. r8 e8\p
                  r1^\markup{
                    \column{
                      \line{\italic{si guarda intorno sorpresa,}}
                      \line{\italic{girando lentamente il capo}}
                  }}
                  a2\pp\< (bes4\mf) r4
                  r8 \xNote b4_\espirare\f r2 \Bfour r8

                  r8  
                  \alniente 
                  g8\ppp\>  (fis2.  
                  \hide DynamicText \scenaBfour g1\ppppp )

  }\addlyrics{ba- sta ba- sta sta tal- men- te ta a 
              ba-sta ba- sta ba- sta?
              qu- u- a- nto ba sta? ba ___ [s] [s] [s]
              [a] [a] [e] ___  [o] [o] [o] [e] po [o] po_-_-_-_-_-_co}


