
quartavoce =
\relative{
%==============A
\stopStaff
  \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  %\diecisec
  %\startGroup
  \alniente
                                     %\A  
                                     \xCircleOnce b1\pp\>  %\hideNH    b  b b b                        \uhideNH \uhideStem
                                          (\xNote b4)
                                          \xCircleOnce  b1\p %\hideNH   b b b b
                                           (\xNote b4)
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\cinquesec
                                          %\startGroup
                                          %\xNote  b\p\< \hideNH   b b b b\mf 
                                          \xCircleOnce b1\p\<  %\hideNH    b  b b b                        \uhideNH \uhideStem
                                          (\xNote b4\mf)
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\cinquesec
                                          %\startGroup
                                          \xCircleOnce b1\pp\<  %\hideNH    b  b b b                        \uhideNH \uhideStem
                                          (\xNote b4\f)
                                         %\xNote  b\<\pp \hideNH  b b b b\f 
                                         %\stopGroup 
                                         %\uhideStem \uhideNH
                                         %\duesec
                                         %\startGroup

                                         \xCircleOnce b2\f %\hideNH  b 
                                         %\stopGroup 
                                        %\pausaPos \pausaLung                                                                                     
                                         r2. %b b b 
                                         %\uhideNH \uhideStem
                                         %\revStemY \revStemLung                                       
                                         
                                         %\cinquesec
                                         %\startGroup
                                         \alniente
                                         \once \override TextScript.extra-offset = #'(-4 . 1)

                                         \xCircleOnce b1\f_\inspirare\> %\hideNH  b b b b 
                                         (\xNote b4)
                                         %\stopGroup 
                                         %\uhideNH \uhideStem
                                         %\tresec
                                         %\startGroup
                                         \xCircleOnce b2.\mf %\hideNH  b b %\stopGroup  
                                         %\pausaPos \pausaLung                                                                                     

                                         r1 r2. r2. %b b b b b b b b b b  \uhideNH \uhideStem
                                         %\revStemY \revStemLung                                       


                                           \xNote b4\f%^\markup{"~1\""}  
                                                                                    %\pausaPos \pausaLung                                                                                     
                                          r1.
                                          % \hideNH  b b b b b b \uhideNH \uhideStem
                                                                            % \revStemY \revStemLung                                       

                                        \xNote b4\mf%^\markup{"~1\""}  
                                         %\pausaPos \pausaLung                                                                                     

                                       r2.% \hideNH  b b b b \uhideNH \uhideStem
                                        %\revStemY \revStemLung
                                        r4
                                        \xNote
                                        b4\f
                                        \xNote
                                        b 
                                        \xNote
                                        b 
                                         %\pausaPos \pausaLung \hideNH  
                                        %b b 
                                         %\revStemY \revStemLung   
                                         r4
%\uhideNH \uhideStem
                                        r4
                                        \xCircleOnce
                                          %\pieno  
                                          b1\f %\hideNH   
                                          %\pausaPos \pausaLung
                                          %b b b b %\stopGroup 
                                          %\uhideNH \uhideStem
                                          (\xNote b4)
                                          \xCircleOnce
                                          %\pieno  
                                          b1\f %\hideNH  b b b 
                                           %\pausaPos \pausaLung \hideNH  
                                        r2 
                                         %\revStemY \revStemLung                                       

%\undo \hide Staff.Clef

                                         % \uhideNH \uhideStem
                                          \xCircleOn
                                          b2.\f%^\markup{\column{\italic{\line{inspirando ed espirando}\line{ su ogni suddivisione}}}}  
                                          %\uhideNH
                                          (b2)
                                         \xCircleOff
                                          \xCircleOnce b2.\glissando\f\>^\markup{\italic{il rumore scompare gradualmente rimanendo solamente il fischio di gola}} 

\stopStaff
  \revert Staff.StaffSymbol.line-count
           \undo \hide Staff.Clef
           
  \startStaff
      \clef "treble"
      \whistleOnce fis'2.\pp 

                                        r2 
 \whistleOn
                                      g2.\p^\markup{\italic{sempre con fischio di gola}}

                                      gis\pp
                                      d'8\mf (dis4) r4.

 \clef "treble_8"
 

  ais,4.\p\<^ \markup{\italic{fischio normale}}
  g8 (fis4 dis2\ff ) r4
  
  a'!4\p\< (ais4 cis4\f)^\markup{\italic{fischio con voce}}
  
  \whistleOff
 \partCombine
 
  { \stemUp \whistleOnce a'2.\f_\markup{[u]}}
  {\stemDown fis2. }
  
  \partCombine
  { \stemDown \whistleOnce fis'2\p_\markup{[u]}}
  {\stemDown fis2. }
  
  \partCombine
  { \stemDown \whistleOnce f'!4\mf_\markup{[u]}}
  {\stemDown f!2. }
  
  

  \whistleOnce
  fis,2.\pp^\markup{\italic{solo fischio}}
  
  \whistleOnce
  
   e'4 (\whistleOnce dis2 )
  
}


%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics{[ʃ]  [tʃ]  [h]     [s]     [tʃ]     [h]     [h]             
                                              [tʃ]        [ʃ]     [s] [s] [s]   [s]     [tʃ]      [h]      [h] }
