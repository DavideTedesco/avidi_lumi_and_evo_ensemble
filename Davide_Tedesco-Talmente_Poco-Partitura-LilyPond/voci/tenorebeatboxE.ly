quartavoce =     
\relative{
  
  r1 r1 r1 r1 e2.\f (dis4) r1 r1 dis\glissando\mf fis\glissando gis r1 r1 a\glissando_\markup{[i]------[e]} cis r2 d r1
  r1 r1  bes r4 bes2. (bes1) (bes2) r2 ees1 e1 e\glissando_\markup{[o]-----------[e]} fis r2. fis4 r2  d (d) r2 r1  r1 
  r2 r4 bes4\f (bes1)\fermata r1 r4 r1 r4
}                                     
                                   \addlyrics {po-o [o] [o] [o] ___  ___ [i]
                                    Tal- me po co ___ ___ [i] [i] po po  }