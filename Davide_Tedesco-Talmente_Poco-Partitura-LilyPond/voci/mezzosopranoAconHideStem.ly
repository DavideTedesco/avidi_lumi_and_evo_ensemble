
secondavoce =
\relative{
  %\override TextScript.outside-staff-priority = #1

%==============A
\stopStaff
   \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  \alniente
  \diecisec
  \startGroup
                                     \A  \xNote b'4\pp\>  \hideNH \hideStem   b  b b b                       \uhideNH \uhideStem
                                          \xNote  b\p \hideNH \hideStem  b b b b \stopGroup \uhideNH \uhideStem
                                          \cinquesec
                                          \startGroup
                                          \xNote  b\mf \hideNH \hideStem  b b b b \stopGroup \uhideNH \uhideStem
                                          \tresec
                                          \startGroup
                                          \xNote  b\mf \hideNH \hideStem  b b  \stopGroup s2 \uhideNH \uhideStem
   \hide Staff.Clef

                                          \xNote  b4\mf \uhideNH \uhideStem
                                          \quasec
                                          \startGroup
                                          \xNote b \hideNH \hideStem b  b b \stopGroup \uhideNH \uhideStem
                                          \tresec
                                          \startGroup
                                          \xNote  b\f \hideNH \hideStem  b b \stopGroup \uhideNH \uhideStem
                                          \quasec
                                          \startGroup
                                          \xNote  b \hideNH \hideStem  b b b  \stopGroup \uhideNH \uhideStem
                                          \xNote b^\markup{"~1\""} \hideNH \hideStem  \uhideNH \uhideStem
                                          \xNote  b^\markup{"~1\""}\mf \hideNH \hideStem s4  \uhideNH \uhideStem
                                          s2
                                          \duesec
                                          \startGroup
                                           \xNote b4\f \hideNH \hideStem b \stopGroup \uhideNH \uhideStem
                                           \xNote  b^\markup{"~1\""} \hideNH \hideStem   \uhideNH \uhideStem

                                           s4
                                           \xNote
                                           b4\mp^\markup{"~1\""}  \hideNH \hideStem s2. s1 \uhideNH \uhideStem
s2.
                                          \xNote b4\ff^\markup{"~1\""}  \hideNH \hideStem s2 \uhideNH \uhideStem
s1 s4
                                              \undo \hide Staff.Clef

\stopStaff
  \revert Staff.StaffSymbol.line-count

  \startStaff

       << \xNote  b4\p f  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem

\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \startStaff
               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
  \stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
             << \xNote  b\p e  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem
s1 s2
}

%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[f] ___ ___ ___ ___ [f] ___ ___ ___ ___ [tʃ] ___ ___ ___ ___ [f] ___  ___
                                                       [ʃ] [s] ___ ___ ___ [h] ___ ___ [h] ___ ___ ___ [h] [s]  [ʃ] ___ [tʃ]
                                                       [s] [ʃ]}
