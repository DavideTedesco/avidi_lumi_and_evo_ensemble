
secondavoce =
\relative{
  r1 r1 r1 f'1\glissando\mf gis r1  a1\mf a (a a a) r1  c2 r1 c2 e, r1 r2
  r1 r2 dis4\f (d! cis2 d!8) r8 r4 b'1\glissando d r1 r1 r2 b2 (b1\glissando)_\markup{ [e]-----------[o]} cis
  r2. cis4 r1 r1 r1 r1
  r4 cis2.\f (cis1)\fermata 
  \time 5/4
  \xNote b4^\markup{\italic{con espressione seccata}} 
  r1 r1 r4
}                                     
                                   \addlyrics {[m] [m] Tal-- men---te [o] po- co e [m] ___  me ___  [o]  po basta}