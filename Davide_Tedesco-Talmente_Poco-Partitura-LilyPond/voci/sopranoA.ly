
 %mostraBarlineTratt = \once \undo \hide Score.BarLine \bar "!"

primavoce =     
\relative{
%==============A
%\override TextScript.outside-staff-priority = #1
\tempo 4=70
\time 5/2
  \hide Staff.Clef

\stopStaff
  \override Staff.StaffSymbol.line-count = #1

  \startStaff 
  %\colorBracket #0 #1 #grey
  %\diecisec
  \alniente
                                     \A  %\startGroup  \scenaA
                                     
 %\xNote
 %\pieno 
  \xCircleOnce b'1\pp\>^\markup{\italic{Gli esecutori aprono gli occhi lentamente prima di iniziare.}}%^\markup{\box{A.1}} 
                                         %\pausaPos \pausaLung

                                     %\hideNH   
                                     %b  b b b
                                     (\xNote b4)

%\uhideNH \uhideStem
                                          %\revStemY
                                          %\revStemLung
                                                                                   

                                          \xCircleOnce
                                          %\pieno  
                                          b1\p %\hideNH   
                                          %\pausaPos \pausaLung
                                          
                                          ( \xNote b4) %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\xCircleOff
                                          %\cinquesec
                                         % \startGroup
                                         %\revStemY
                                          %\revStemLung
                                         \xCircleOnce
                                          b1\mf 
                                         %\hideNH
                                         %\pausaPos \pausaLung
                                        
                                         (\xNote b4)  
                                         %\xCircleOff
                                         %\stopGroup 
                                         %\uhideNH \uhideStem 
%\break
%\tresec
                                          %\startGroup
                                          %\break       
                                         
                                         %\revStemY
                                          %\revStemLung
                                        %\xNote
                                        %\pieno 
                                        \xCircleOn
                                        b2.\mf%^\markup{\box{A.2}} 
                                        %\pausaPos \pausaLung

                                         %\hideNH 
                                         
                                         %\stopGroup 
                                         
                                         %\pausaPos \pausaLung
                                          r2
                                         %\uhideNH \uhideStem
                                         %\revStemY
                                         %\revStemLung
\break
                     % \set Staff.pedalSustainStyle = #'bracket
                                                
                                                % \once \undo \hide Score.BarLine \bar "!"
                                                
                                        %\once \override TextScript.extra-offset = #'( 0 . -0.9 )
                                        \xCircleOff
                                        \xNote  b4\mf%^\parentesiUnSec^\unsec^\markup{\dynamic{mf}} % \hideNH \hideStem  \stopGroup \uhideNH \uhideStem
                                        %\duesec 
                                        %\startGroup
                                        \xCircleOn
                                         b2 
                                         %\hideNH  b 
                                        %\stopGroup  
                                        %\pausaPos \pausaLung
                                        \xCircleOff
                                        r2
                                        %\uhideNH \uhideStem
                                        %\revStemY
                                        %\revStemLung

                                           
                                        %\once \override TextScript.extra-offset = #'( 0 . 0.2 )
                                        \xNote  b4%^\markup{\box{A.3}}%^\parentesiUnSec^\unsec
                                        %\pausaPos \pausaLung
                                        %\hideNH   
                                        r4 
                                        %\uhideNH \uhideStem 
                                        %\revStemY
                                        %\revStemLung

                                        \xNote  b4%^\parentesiUnSec^\unsec
                                          %\pausaPos \pausaLung

                                        %\hideNH  
                                        r4 
                                        %\uhideNH \uhideStem
                                         %\revStemY
                                        %\revStemLung

                                          %\duesec
                                          %\startGroup
                                          \xNote  b4 \xNote b4\f %\hideNH   b %\stopGroup  
                                          %\pausaPos \pausaLung                                          
                                          r2 % b4\uhideNH \uhideStem 
                                           % \revStemY
                                           % \revStemLung                                       
                                           %\duesec
                                           %\startGroup
                                           \xCircleOnce  b2\mf%^\markup{\box{A.4}}
                                           %\hideNH  b 
                                           %\stopGroup 
                                          %\pausaPos \pausaLung                                                                                     
                                           r2 %b4 b4  \uhideNH \uhideStem
                                           %\revStemY \revStemLung                                               
                                           \xNote  b4\f%^\parentesiUnSec^\unsec^\markup{\dynamic{f}} 
                                         % \pausaPos \pausaLung                                                                                     
                                           r4%\hideNH  b4 \uhideNH \uhideStem
                                           %\revStemY \revStemLung                                               
                                           
                                           \xNote  b4%^\parentesiUnSec^\unsec 
                                          % \hideNH   \uhideNH \uhideStem
                                              %\once \hide Staff.Clef
\time 7/2
                                           \xNote b4\p%^\markup{"~1\""}^\markup{\box{A.5}}  
                                           %\hideNH 
                                           %\pausaPos \pausaLung

                                           r1 r1 r1%^\markup{\box{A.6}}  %\uhideNH \uhideStem
                                          %\revStemY
                                          %\revStemLung

                                           \xNote b4\fff%^\markup{"~1\""} 
\time 5/4                                           
                                          \xNote b4\f%^\markup{\box{A.7}}\f %\hideNH 
                                          
                                         % \pausaPos \pausaLung
                                         r2 % b b 
                                            %                                        \revStemY
                                         % \revStemLung
                                          %\uhideNH
                                          \xNote  b4 
                                         % \pausaPos \pausaLung
                                          %\hideNH	
                                          r4 %b b %\uhideNH \uhideStem
                                         r4
                                         %\revStemY
                                          %\revStemLung
                                          %\uhideNH
                                          \xCircleOnce
                                          b1^\markup { \italic più \dynamic f } %\hideNH b b b
                                          %\pausaPos \pausaLung
                                         % \hideNH	
                                          r4%b  \uhideNH \uhideStem
                                          %\revStemY
                                          %\revStemLung
                                          \xCircleOnce
                                          b1^\markup{\italic{rall. molto}}%^\markup{\box{A.8}} %\hideNH b b b
                                          %\pausaPos \pausaLung
                                          %\hideNH	
                                          r4 %b4  \uhideNH \uhideStem
                                         % \revStemY
                                          %\revStemLung
                                          
                                           \xCircleOnce  b1\ff %\hideNH   b b b  \uhideNH \uhideStem
%\break
\tempo 4=50
               \xNote  b4\f%^\markup{\box{A.9}}%^\markup{\italic{a tempo}}   \uhideNH \uhideStem
               \alniente
               \xCircleOn  b1\f\>% \hideNH  b b  b\once \hide Staff.DynamicText
               (\once \hide DynamicText \xNote b4)\p   
               \xCircleOff
               % \uhideNH \uhideStem
                                                    %     \pausaPos \pausaLung
                                       %   \hideNH	
                                         r1 % b b b  b \uhideNH \uhideStem
                                          %\revStemY
                                          %\revStemLung
                                             \undo \hide Staff.Clef            
                                             
\tempo \markup {
    \concat {
      
      \smaller \general-align #Y #DOWN \note {4} #1
      " = "
      \smaller \general-align #Y #DOWN \note {4.} #1
      
    }
  }
\time 6/8

\stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
\break
r2.
\xNote b4\glissando\f\>^\noiseToSound%^\markup{\box{A.10}} 
d2\p
       
\xNote b4\glissando\p\< d2\ff

\xNote b4\glissando\mf e2

r2.

ais,2\p%^\markup{\box{A.11}}
r4

a4\f f2_.

d'2.

\alniente  f2.\p\> (f2.)\hide DynamicText r2.\p
}

                                                          
%==============TESTO                                     
                                   \addlyrics {[ʃ] [s] [f] [ʃ] [tʃ] [s] [h] [h] [h] [f] [s] [f] [s] [tʃ]
                                                     [tʃ] [ʃ] [tʃ] [s] [ʃ] [ʃ] [s] [ʃ] [s]- [z] [ʃ]- [g]  [f]- [v] [m]}