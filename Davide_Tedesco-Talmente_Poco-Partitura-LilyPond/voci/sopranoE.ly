

primavoce =     
\relative c''{
     \tempo 4=70
  \time 4/2
  \E
  b1\glissando\pp g\glissando c\glissando c (c\glissando) d
   r1 r1 e4\mf (d2\glissando) c\glissando b r4 bes1\f\> (bes) (bes\pp) 
   r2 bes\mp\glissando 
   \tempo 4=90
   g1 
   r1 
   r1 c2\glissando\f a2 (fis1)
   r1 e' (e2) r2 
   \break
   r1 r1 d1\glissando_\markup{[u]-----------[a]} e
   \tempo 4=50
   \time 4/4
    e1 r1 r1 a,4 (g fis2) gis2 r4 r4 e 
    e1\f 
    (e1)\fermata
    r1 r4 r1
}                                     
                                   \addlyrics {[m] [m] [m] [m] [m] e-en te- [e] so [i] [i]  [i] [i] [a] ___ ___ [o] so [o] po d}