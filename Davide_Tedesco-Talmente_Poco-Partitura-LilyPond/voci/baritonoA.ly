


quintavoce =
\relative{
%==============A
\stopStaff
  \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  \alniente
%\cinquesec
  %\startGroup
                                     %\A   
                                     \xCircleOnce d1\pp\> % \hideNH  d  d d d 
                                     (\xNote d4)
                                     %\stopGroup                    
                                     %\uhideNH \uhideStem
                %\diecisec
  %\startGroup
                                          \once \override TextScript.extra-offset = #'(-3 . 0)

                                            \xCircleOnce d2.\p\<_\espirare 
                                            %\hideNH   
                                           % d  d_\inspirare d d  \uhideNH \uhideStem
                                          (\once \override TextScript.extra-offset = #'(-3 . 0)

                                           \xCircleOnce d2_\inspirare)
                                       \alniente
                                       \once \override TextScript.extra-offset = #'(-3 . 0)

                                       \xCircleOnce  d2._\espirare\mf\> 
                                       %\hideNH   
                                      (\once \override TextScript.extra-offset = #'(-3 . 0)

                                      \xCircleOnce d2_\inspirare)
                                       %d d 
                                       %\stopGroup  
                                       %\uhideNH \uhideStem
                                        %\cinquesec
                                        %\startGroup
                                          \xCircleOnce  d2.\pp\<  (\xCircleOnce d2\f)
                                          %\hideNH  d  d d d\f   
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\duesec
                                        %\startGroup

                                          \xCircleOnce  d2\f 
                                          %\hideNH   d   
                                          %\stopGroup 
                                          %\pausaPos \pausaLung                                                                                     
                                          r2.
                                          %d d d \uhideNH \uhideStem
                                         %\revStemY \revStemLung                                       
                                          
                                          \xNote  d4\mf
                                          %^\parentesiUnSec^\unsec^\markup{\dynamic{mf}} 
                                                                                    %\pausaPos \pausaLung                                                                                     
                                          r4
                                          %\hideNH   d \uhideNH \uhideStem
                                                                                   %\revStemY \revStemLung                                       

                                          \xNote d4
                                          %^\parentesiUnSec^\unsec 
                                          %\hideNH  
                                           %                                         \pausaPos \pausaLung                                                                                     

                                          r4%d \uhideNH \uhideStem
                                                                                   %\revStemY \revStemLung                                       

                                          %\duesec
                                          %\startGroup
                                          \xNote  d4\f (\xNote d4)
                                          %\hideNH   d 
                                          %\stopGroup 
                                                                     %\pausaPos \pausaLung                                                                                     

                                          r1%d d d d \uhideNH \uhideStem 
                                                                     %\revStemY \revStemLung                                       

                                          %\duesec
                                          %\startGroup
                                          \xCircleOnce  d2\f 
                                          %\hideNH   d 
                                          %\stopGroup 
                                          %\uhideNH \uhideStem

                                          %\tresec
                                          %\startGroup
                                          \xCircleOn d2.
                                          %\hideNH   d d 
                                          \xCircleOff
                                          %\stopGroup 
                                                                                                               %\pausaPos \pausaLung                                                                                     

                                          r1
                                          %d d d d \uhideNH \uhideStem 
                                                                                                       %\revStemY \revStemLung                                       
     

                                      \xNote d4\ff
                                      %^\markup{"~1\""}  
                                                                                                                                                     %\pausaPos \pausaLung                                                                                     

                                      r1
                                      %\hideNH  d d d d \uhideNH \uhideStem
                                                                                                                                             %\revStemY \revStemLung                                       

                                      \xNote d4\p%^\markup{"~1\""}  
                                                                                                                                             % \pausaPos \pausaLung                                                                                     
                                       r1
                                       r1 r4
                                       r1 r4
                                      %\hideNH  d d d d d d d d d d d d d d d \uhideNH \uhideStem
                                                                                                                                             %\revStemY \revStemLung                                       
                                       \xCircleOn d2.\f %\hideNH d d  \uhideNH
                                        d2% \hideNH d d d  \uhideNH
                                       ( d2.)
                                        d2\ff %\hideNH d d d d  \uhideNH
                                       ( d2.)
                                        d2\mf %\hideNH d    \uhideNH
                                       \xCircleOff
                                      %(\xNote d)
                                      %\pausaPos \pausaLung                                                                                     
                                        %\hideNH 
                                         r1 r4
                                         %d4 d d d d
                                       

                                              \undo \hide Staff.Clef
\stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
    r2.
    \xNote b4\glissando\f\>^\noiseToSound dis2\p
    
    d8\mf (e4) r4.
    
    \xNote b4\glissando\p\<^\noiseToSound dis2\f

  e2\ff r4
  
  dis2\p r4
  
  a2.
  
  bes
  
\alniente  f2.\p\> (f2.)\hide DynamicText r2.\p
}

%==============TESTO
\addlyrics {[tʃ]  [ʃ]     [ʃ]      [tʃ]     [tʃ]     [h]  [h]  [ʃ]      [tʃ]  [s]       [tʃ]     [s] [ʃ] [s] [tʃ] [ʃ]
                    [s]- [m] [m] [s]- [z] [m] [m]}
