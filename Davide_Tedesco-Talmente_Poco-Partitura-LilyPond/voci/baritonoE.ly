


quintavoce =
\relative{
  r1 r1 r2 d2\glissando\mp e1\glissando cis (cis)
  r1 r1 r1 c4 (cis2.)
  r4 cis1\glissando_\markup{[i]---------[o]}\p\< dis1 dis1\f  
  r8 a8  a2 
  a1

  %\afterGrace 1/16 a1 {a8}
  r1 r1 r2 r4 d4\f
  dis1 r1 r1 d2 c2 (c2) c1. 
%  \once \override TextScript.extra-offset = #'(-4. 6)
  c1_\markup{[a]------------[i]} \glissando
  d
  r1 d1 (d1) r1 r1 r1 g\f r1 r4 r1 r4
}                                     
                                   \addlyrics {[o] [o] [o] [o] ___ ___ ___ so li  ______i-____ttle Tal me te [i] po ___ ___ [i] [o]}