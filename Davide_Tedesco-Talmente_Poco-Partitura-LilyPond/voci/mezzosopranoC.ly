
secondavoce =
\relative{
    \override Staff.NoteHead.style = #'triangle

  %\override TextScript.outside-staff-priority = #1

%==============C
\stopStaff
   \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  r1^\markup{tacet}^\markup {
    \column{
    \line{\italic{La Ms. chiude gli occhi}}
    \line{\italic{dopo aver raggiunto}}
    \line{\italic{lentamente la scena } \box{C}}
  }}
  \scenaC

s4

s1*15
s4

b'2^\bia^\markup{\italic{apre gli occhi}}
b2.^\bia
r2.
r2.
r2.
r2.
b2^\pugno r2
b1^\bia
r2. 
\once \override TextScript.extra-offset = #'(-2 . 0)                                                                 
b4^\antiorario
\once \override TextScript.extra-offset = #'(0 . -4)                                                                 
b2.^\bia r4
r4 b4^\mani_\giu b4_\su r4
r1 r4 
r4 b1^\bia 
b4^\pugno r1
b4^\pugno r4
b2^\biagia
b2^\bia
b2^\biagia b2^\bsc
b1^\strofinare
b1 b1
b1^\orario
}
