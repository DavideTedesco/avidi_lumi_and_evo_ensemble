
secondavoce = \relative {                                    
%==============F    
                          { r1 \alniente ais'1~\p\> ais2~ais\fermata  
                          dis1\mp\< \glissando(
                           e\glissando 
                         \once \override TextScript.extra-offset = #'(-1 . 5.5)                          
                           fis_\updownarrow \f) 
                           \breathe                                                                
                           dis1\mp\< \glissando(
                           d!) 
                         (\once \override TextScript.extra-offset = #'(-1 . 4.5)                          
                           d_\updownarrow \f) 
                           \breathe  
                           ais\p\< (ais ais\f)
                           \breathe
                           a!\pp
                           \breathe  
                           ais\mf\glissando (
                           \once \override TextScript.extra-offset = #'(-1 . 3)                                                                         
                           f!_\updownarrow)
                           r1
                           \breathe
                           r4 b4\mf\glissando (
                           \once \override TextScript.extra-offset = #'(-1 . 3.5)                                              
                           bes2_\updownarrow )
                           \breathe
                           c2\p\<\glissando (
                           \once \override TextScript.extra-offset = #'(-1 . 3.5)                                              
                           b_\updownarrow)
                          \breathe
                           \alniente r4\f\>  c2.  (c1)
                           \breathe
                             \alniente f2\f\>\glissando (fih\glissando)(
                                     \once \override TextScript.extra-offset = #'(-1 . 5.5)                                              
                                    f_\updownarrow ) r2
                           \breathe
                           gis,\glissando 
                           (\once \override TextScript.extra-offset = #'(-1 . 3.2)                                                             
                           g_\updownarrow)
                           a^\markup{\column{\line {\italic{si guarda intorno}}\line {\italic{con circospezione}}}}\pp 
                            r2
                          \breathe
                           fis1\mp\< (fis\ff) \fermata
                           r1
                           \bar  "|."}}
%==============TESTO    

                          \addlyrics {
                            %B
                              %ba- sta ta [a] ta [a] te [e] ta [a] 
                              %ba [a] [a] ta [a] sta?
                              %qu [u] [a] [o] [a] [a?] [a] ___ [s] [s] [s]
                              %[a] [a] [e] ___  [o] [o] [o]        
                            %F
                            ta me [e] [o] [o] [a] [o] [u] [e] [a] [e] [a] [a]---------[o]}
                                                                                                                 %\addlyrics{ a e o o u}\addlyrics{ o}
