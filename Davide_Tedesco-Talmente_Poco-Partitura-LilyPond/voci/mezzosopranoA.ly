
secondavoce =
\relative{
  %\override TextScript.outside-staff-priority = #1

%==============A
\stopStaff
   \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  \alniente
  %\diecisec
  %\startGroup
                                    % \A  
                                     \xCircleOnce b'1\pp\>  %\hideNH    b  b b b                       \uhideNH \uhideStem
                                          (\xNote  b4) %\hideNH   b b b b 
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\cinquesec
                                          %\startGroup
                                          \xCircleOnce b1\p  %\hideNH    b  b b b                       \uhideNH \uhideStem
                                          (\xNote  b4) %\hideNH   b b b b 
                                          %\tresec
                                          %\startGroup
                                          \xCircleOnce b1\mf  %\hideNH    b  b b b                       \uhideNH \uhideStem
                                          (\xNote  b4) %\hideNH   b b b b 
                                          %\stopGroup 
                                         % \uhideNH \uhideStem
   \hide Staff.Clef

                                          %\once \override TextScript.extra-offset = #'( 0 . -0.9)
                                          \xCircleOnce  b1\mf%^\parentesiUnSec^\unsec^\markup{\dynamic{mf}}
                                          (\xNote b4)
                                          %\quasec
                                          %\startGroup
                                          \xNote b4\mf 
                                          %\xNote b \hideNH  
                                          %b  b b 
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\tresec
                                          %\startGroup
                                          \xCircleOnce  b1 %\hideNH   b b 
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\quasec
                                          %\startGroup
                                          \xCircleOnce  b2.\f% \hideNH   b b b  
                                          %\stopGroup 
                                          %\uhideNH \uhideStem
                                          %\once \override TextScript.extra-offset = #'( 0 . -1.568 )

                                           \xCircleOn  b2%^\parentesiUnSec^\unsec 
                                           %\hideNH   \uhideNH \uhideStem
                                           %\once \override TextScript.extra-offset = #'( 0 . -1.568 )
                                             (b2)%^\parentesiUnSec^\unsec^\markup{\dynamic{mf}} 
                                           \xCircleOff
                                           % \pausaPos \pausaLung
                                           \xNote b4
                                           \xCircleOnce b1\mf
                                           % r4 r2
                                           %\hideNH  b4 b4 b4 \uhideNH \uhideStem
                                            %\revStemY \revStemLung                                       

                                          %\duesec
                                          %\startGroup
                                           \xCircleOnce b2\f% \hideNH  b 
                                           %\stopGroup 
                                           %\uhideNH \uhideStem
                                           \xNote  b4%^\markup{"~1\""}
                                           %\pausaPos \pausaLung                                          
                                          r4
                                           %\hideNH b   \uhideNH \uhideStem
                                           % \revStemY \revStemLung                                       

                                           
                                           \xNote
                                           b4\mp%^\markup{"~1\""}  
                                           %\hideNH  
                                           %\pausaPos \pausaLung  
                                           r1
                                           r1
                                           r2
                                           %b b b b 
                                           %b b b b 
                                           %b b \uhideNH \uhideStem
                                            %\revStemY \revStemLung                                       

                                          \xNote b4\ff%^\markup{"~1\""}  
                                          %\hideNH 
                                           %\pausaPos \pausaLung                                                                                    
                                          r4 r2%b b b 
                                           %\revStemY \revStemLung                                       
%\uhideNH
\xNote
                                          b4\f 
                                         % \hideNH
                                        % \pausaPos \pausaLung                                                                                    

                                          r2 r4%b b b \uhideNH \uhideStem
                                          %  \revStemY \revStemLung                                       
                                          \xNote
                                          b
                                          %\pausaPos \pausaLung                                                                                    
       % \hideNH
                                          r2. %b b b \uhideNH \uhideStem
                                           % \revStemY \revStemLung 
                                            
                                         \xCircleOnce
                                          b1\f 
                                          %\hideNH
                                          %b b b b b
                                          %\uhideNH
                                         (\xNote b4 \xNote b4)
                                          
                                          \xCircleOnce b1\f 
                                          %\hideNH
                                          %b b b
                                          %\uhideNH
                                          \xCircleOff
  \override Staff.StaffSymbol.line-count = #1
  \startStaff
               \xCircleOnce  b1\f 
               %\hideNH   b b  b \uhideNH \uhideStem
                \xNote b4\mp  ( \xCircleOnce b2) %b
                                                        % \pausaPos \pausaLung                                                                                    
  r2.
               %b b b% \uhideNH \uhideStem
                                            %\revStemY \revStemLung 
  \stopStaff
                                                \undo \hide Staff.Clef

  \revert Staff.StaffSymbol.line-count
  \startStaff
  r2.
\xNote b4\glissando\f\>^\noiseToSound cis2\p

\xNote b4\glissando\p\< dis2\ff

\xNote b4\glissando\mf fis2

r2.

a2.\p

g4\f g2-.

cis2.

\alniente  f2.\p\> (f2.)\hide DynamicText r2.\p
}

%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[f] [f] [tʃ] [f]
                                                            [ʃ] [s]    [h]   [h]    [h] [s]     [ʃ]  [tʃ]
                                                          [s]           [ʃ]   
                                                         [s]     [ʃ] [tʃ] [s] [ʃ] [ʃ] [f]- [v] [ʃ]- [g] [f]- [v] [m]}
