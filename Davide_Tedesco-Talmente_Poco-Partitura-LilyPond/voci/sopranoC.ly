
 %mostraBarlineTratt = \once \undo \hide Score.BarLine \bar "!"

primavoce =     
\relative c''{
  \override Staff.NoteHead.style = #'triangle

%==============C
%\override TextScript.outside-staff-priority = #1

  \hide Staff.Clef
\tempo 4=30
\stopStaff
  \override Staff.StaffSymbol.line-count = #1

  \startStaff 
                                 
                                 \C  
                                 r1 r4 
                                 r1 r4 r1 r4 r1 r4 
\tempo 4=50                                 
                                 \tuplet 6/5 {b4^\mani^\markup{\italic{apre gli occhi}}_\giu b4_\su b4_\giu b4_\su b4_\giu   b4_\su}
                                  r1 r4 
 \time 3/4
                                  r2.             
                                  b8^\mani_\giu b8_\su b8_\giu b8_\su r4
                                 \tuplet 4/3 {b4^\mani_\giu b4_\su b4_\giu b4_\su}
                                 r2.
\tempo 4=60                   
                                 r2.
                                 b2^\orario r4
                                 b8^\mani_\giu b8_\su b8_\giu  r4.
                                 r2.
\time 2/4
                                 b8^\pugno r8 r4
                                 r8 b8^\pugno r4
                                 b2^\bia
                                \once \override TextScript.extra-offset = #'(2 . -1)                                                                 
                                 r4^\orario
                                 b4
                                 %\once \override TextScript.extra-offset = #'(-1. 0)                                 
                                 b4^\mano_sinistra_\su 
                                 r4
                                 b2^\bia
                                 r2 \mark \markup{\column{
                                   \line{\italic{spostandosi}}
                                   \line{\italic{velocemente}}
                                   \line{\italic{raggiungere} \box {D1}}
                                 }} 
                                 
\tempo 4=30
\time 3/4
                                 b2.^\bia \scenaDone
                                 r2.
                                 r2.
                                 r2.
                                 r2. \mark \markup{\column{
                                   \line{\italic{raggiungere} \box {D2}}
                                 }} 
 \time 4/4
                                  b4^\pugno  b4 r4   r4 \scenaDtwo
                                  b1^\bia
                                  b2^\antiorario r2
                                  b2.^\bia r4
                                  b4^\mani_\giu b4_\su r2
\time 5/4
                                   r1 \mark \markup{\column{
                                   \line{\italic{raggiungere} \box {D3}}
                                 }}  r4 \scenaDthree
                                 
                                 b1^\bia (b4) 
                                 r1 r4
\time 2/4
\tempo 4=20
                                 b4^\pugno \mark \markup{\column{
                                   \line{\italic{raggiungere} \box {D4}}
                                 }}  r4 \scenaDfour
                                 b2^\biagia
                                 b2^\bia
 \time 4/4                 
                                 r1
  \tempo 4=60                               
                                 b1^\strofinare \mark \markup{\column{
                                   \line{\italic{raggiungere} \box {D5}}
                                 }} 
                                 b1^\markup{\italic{velocizzando molto}} \scenaDfive b1 
                                 b1^\orario
                                 

}