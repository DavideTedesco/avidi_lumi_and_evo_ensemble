

terzavoce =
\relative{
%==============A
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \hide Staff.Clef
  \startStaff
  \diecisec
  \startGroup
  \alniente
                                     \A  \xNote b4\pp\>  \hideNH \hideStem   b  b b b                       \uhideNH \uhideStem
                                          \xNote  b\p \hideNH \hideStem  b b b b \stopGroup s4\uhideNH \uhideStem
                                          \cinquesec
                                          \startGroup
                                          \xNote  b\p\< \hideNH \hideStem  b b b b\mf \stopGroup \uhideNH \uhideStem

                                         s4
                                         \duesec
                                         \startGroup \xNote b\mf \hideNH \hideStem  b \stopGroup s4
                                         \uhideStem \uhideNH
                                         \cinquesec

                                          \startGroup \xNote b\mf \hideNH \hideStem  b b b b \stopGroup
                                         \uhideStem \uhideNH
                                         \ottosec
                                         \startGroup
                                         \xNote b_\inspirare\p\<  \hideNH \hideStem b b b_\espirare
                                         %\once \override TextScript.extra-offset = #'(0 . -3)
                                         b b b b\f  \stopGroup \uhideNH \uhideStem
                                                                                    %\once \hide Staff.Clef

s1
s1
s4

                                           \xNote b4\mf^\markup{"~1\""}  \hideNH \hideStem s2 \uhideNH \uhideStem
                                            \quasec
                                          \startGroup \xNote b4\mf\< \hideNH \hideStem  b b b\f \stopGroup
                                         \uhideStem \uhideNH
                                         s2
                                          \xNote b4\f^\markup{"~1\""}  \hideNH \hideStem s2 \uhideNH \uhideStem

s1 s2
                                              \undo \hide Staff.Clef

\stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
       << \xNote  b4\p f  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \startStaff
            \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
  \stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
            << \xNote  b\p e  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem
s1 s2
}


%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[ʃ] ___ ___ ___ ___ [tʃ] ___ ___ ___ ___ [h] ___ ___ ___ ___ [ʃ] ___ [s] ___ ___ ___ ___ [h] ___ ___ ___ ___  ___ ___ ___ [s] [tʃ] ___ ___ ___ [ʃ]}
