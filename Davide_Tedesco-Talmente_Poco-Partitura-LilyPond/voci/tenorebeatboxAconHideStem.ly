
quartavoce =
\relative{
%==============A
\stopStaff
  \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  \diecisec
  \startGroup
  \alniente
                                     \A  \xNote b4\pp\>  \hideNH \hideStem   b  b b b                        \uhideNH \uhideStem
                                          \xNote  b\p \hideNH \hideStem  b b b b \stopGroup \uhideNH \uhideStem
                                          \cinquesec
                                          \startGroup
                                          \xNote  b\p\< \hideNH \hideStem  b b b b\mf \stopGroup \uhideNH \uhideStem
                                          \cinquesec
                                          \startGroup
                                         \xNote  b\<\pp \hideNH \hideStem b b b b\f \stopGroup \uhideStem \uhideNH
                                         \duesec
                                         \startGroup

                                         \xNote b\f \hideNH \hideStem b \stopGroup \uhideNH \uhideStem
                                            s2 s4
                                         \cinquesec
                                         \startGroup
                                         \xNote b\f_\inspirare \hideNH \hideStem b b b b \stopGroup \uhideNH \uhideStem
                                         \tresec
                                         \startGroup
                                         \xNote b\mf \hideNH \hideStem b b \stopGroup \uhideNH \uhideStem
                                         s1
s1
s2

                                           \xNote b4\f^\markup{"~1\""}  \hideNH \hideStem s1 s4 \uhideNH \uhideStem
                                    s4
                                        \xNote b4\mf^\markup{"~1\""}  \hideNH \hideStem s1 \uhideNH \uhideStem

s1 s4
                                              \undo \hide Staff.Clef

\stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
       << \xNote  b4\p f  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \startStaff
               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
  \stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
             << \xNote  b\p e  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem
             s1 s2
}


%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[ʃ] ___ ___ ___ ___ [tʃ] ___ ___ ___ ___ [h] ___ ___ ___ ___ [s] ___ ___ ___ ___ [tʃ] ___ [h] ___ ___ ___ ___ [h] ___ ___   [tʃ] [ʃ] [r̥]}
