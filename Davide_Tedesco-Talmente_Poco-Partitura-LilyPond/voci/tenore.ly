
terzavoce = \relative {
        
%==============F 
                       \bar "!"r1 r2 \alniente b2\p\> (b1)\fermata  
                       d\glissando\mp\< (
                       dis \glissando
                       \once \override TextScript.extra-offset = #'(-1 . 23/5)
                       e_\updownarrow\f)  
                        \breathe                                                            
                       d!\glissando\mp\< (
                       dis \glissando
                       \once \override TextScript.extra-offset = #'(-1 . 23/5)
                       d!_\updownarrow\f)  
                       \breathe
                       f,!\mp\<\glissando (
                       fis\glissando 
                       \once \override TextScript.extra-offset = #'(-1 . 3)                                            
                       fis\f_\updownarrow)
                       r1
                       \breathe
                       f\mf\glissando (
                         \once \override TextScript.extra-offset = #'(-1 . 3)                                                                     
                       fis_\updownarrow)
                       r1
                       \breathe
                       g2\pp\<\glissando (
                         \once \override TextScript.extra-offset = #'(-1 . 3.5)                                              
                         ais\mf_\updownarrow )
                       \breathe
                         c\p\<\glissando (
                         \once \override TextScript.extra-offset = #'(-1 . 0.5)                                                                       
                         c,_\updownarrow)
                        \breathe
                        \alniente r2\f\>  b'2  (b1)
                        \breathe
                       \alniente fis\f\>
                       \breathe
                       r4 e2.\pp
                       \breathe
                       fis2\glissando (
                       \once \override TextScript.extra-offset = #'(-1 . 3.2)                                                                       
                       g_\updownarrow)
                       r1
                      \breathe
                       f?1\mp\< (f\ff) \fermata
                       r1
%==============TESTO    
                       \bar  "|."}\addlyrics {me [e] [e] [o] [a] [o] [u] [e] [o] [o] [o] [o]}