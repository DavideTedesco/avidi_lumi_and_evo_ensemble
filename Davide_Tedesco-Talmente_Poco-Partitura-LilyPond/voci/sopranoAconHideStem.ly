
 %mostraBarlineTratt = \once \undo \hide Score.BarLine \bar "!"

primavoce =
\relative{
%==============A
%\override TextScript.outside-staff-priority = #1

\time 5/1
  \hide Staff.Clef

\stopStaff
  \override Staff.StaffSymbol.line-count = #1

  \startStaff
  \diecisec
  \alniente
                                     \A  \startGroup
 \xNote b'\pp\>^\markup{\italic{Gli esecutori aprono gli occhi lentamente prima di iniziare.}}^\markup{\box{A.1}}

                                     \hideNH \hideStem   b  b b b

\uhideNH \uhideStem

                                          \xNote  b\p \hideNH \hideStem  b b b b \stopGroup  \uhideNH \uhideStem

                                          \cinquesec
                                          \startGroup
                                         \xNote  b\mf \hideNH \hideStem  b b b b  \stopGroup \uhideNH \uhideStem
%\break
\tresec
                                          \startGroup
                                          %\break
                                         \xNote \scenaA b\mf^\markup{\box{A.2}} \hideNH \hideStem  b b  \stopGroup s2\uhideNH \uhideStem
                                                                                                                     % \once \undo \hide Score.BarLine \bar "!"


                                        \xNote  b4->^\markup{"~1\""}\mf
                                        \duesec
                                        \startGroup
                                        \xNote b \hideNH \hideStem b \stopGroup \uhideNH \uhideStem

                                          s2
                                          \xNote  b4^\markup{"~1\""}^\markup{\box{A.3}} \hideNH \hideStem  s4 \uhideNH \uhideStem
                                          \xNote b^\markup{"~1\""} \hideNH \hideStem s4 \uhideNH \uhideStem
                                          \duesec
                                          \startGroup
                                          \xNote  b\f \hideNH \hideStem  b \stopGroup \uhideNH \uhideStem s2
                                           \duesec
                                           \startGroup
                                           \xNote  b4\mf^\markup{\box{A.4}} \hideNH \hideStem b \stopGroup  \uhideNH \uhideStem
                                            s2
                                           \xNote b4\f^\markup{"~1\""} \hideNH \hideStem s4 \uhideNH \uhideStem
                                           \xNote b4^\markup{"~1\""} \hideNH \hideStem \uhideNH \uhideStem
                                              \once \hide Staff.Clef

                                           \xNote b4\p^\markup{"~1\""}^\markup{\box{A.5}}  \hideNH \hideStem \uhideNH \uhideStem
                                            s1 s1
s1^\markup{\box{A.6}}

                                           \xNote b4\fff^\markup{"~1\""} \hideNH \hideStem \uhideNH \uhideStem

s1^\markup{\box{A.7}}

s2
                                              \undo \hide Staff.Clef

\stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff

       << \xNote  b4\p^\markup{\box{A.8}} f  >> \hideNH \hideStem  b b b  \uhideNH \uhideStem
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \startStaff

               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem

               \xNote  b\p \hideNH \hideStem  b b b  \uhideNH \uhideStem
  \stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff

             << \xNote  b\p e  >> \hideNH \hideStem  b b b^\markup{\box{A.9}} s2  \uhideNH \uhideStem
s2^\markup{\box{A.10}}
s2^\markup{\box{A.11}}

}


%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[ʃ] ___ ___ ___ ___ [s] ___ ___ ___ ___ [f] ___ ___ ___ ___ [ʃ] ___  ___ [tʃ] [s] ___ [h] [h] [h] ___ [f] ___ [s] [f] [s] [tʃ]}
