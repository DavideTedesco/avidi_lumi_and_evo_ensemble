


quintavoce =
\relative{
%==============A
\stopStaff
  \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  \alniente
\cinquesec
  \startGroup
                                     \A   \xNote d4\pp\>  \hideNH \hideStem d  d d d  \stopGroup                    \uhideNH \uhideStem
                \diecisec
  \startGroup
                                       \posRespiro     \xNote d4\p\<_\espirare \hideNH \hideStem  d \posRespiro d_\inspirare d d  \uhideNH \uhideStem
                                       \alniente
                                      \posRespiro \xNote  d4_\espirare\mf\> \hideNH \hideStem  d  \posRespiro d_\inspirare d d \stopGroup  \uhideNH \uhideStem
                                        \cinquesec
                                        \startGroup
                                          \xNote  d\pp\< \hideNH \hideStem d  d d d\f   \stopGroup \uhideNH \uhideStem
                                          \duesec
                                        \startGroup

                                          \xNote  d\f \hideNH \hideStem  d   \stopGroup  \uhideNH \uhideStem s2 s4
                                          \xNote  d4^\markup{"~1\""}\mf \hideNH \hideStem  s4 \uhideNH \uhideStem
                                          \xNote d^\markup{"~1\""} \hideNH \hideStem s4 \uhideNH \uhideStem
                                          \duesec
                                          \startGroup
                                          \xNote  d\f \hideNH \hideStem  d \stopGroup \uhideNH \uhideStem s2
                                          s2
                                          \duesec
                                          \startGroup
                                          \xNote  d4\f \hideNH \hideStem  d \stopGroup \uhideNH \uhideStem

                                          \tresec
                                          \startGroup
                                          \xNote  d4\hideNH \hideStem  d d \stopGroup \uhideNH \uhideStem s2
                                       s2

                                      \xNote d4\ff^\markup{"~1\""}  \hideNH \hideStem s1 \uhideNH \uhideStem
                                      \xNote d4\p^\markup{"~1\""}  \hideNH \hideStem s1 \uhideNH \uhideStem

s1 s2
                                              \undo \hide Staff.Clef

\stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
       << \xNote  d4\p f  >> \hideNH \hideStem  d d d \uhideNH \uhideStem
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \startStaff
               \xNote  d\p \hideNH \hideStem  d d d   \uhideNH \uhideStem
               \xNote  d\p \hideNH \hideStem  d d d   \uhideNH \uhideStem
  \stopStaff
  \revert Staff.StaffSymbol.line-count
  \startStaff
             << \xNote  d\p e  >> \hideNH \hideStem  d d d  \uhideNH \uhideStem
             s1 s2
}


%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[tʃ] ___ ___ ___ ___ [ʃ] ___ ___ ___ ___ [ʃ] ___ ___ ___ ___  [tʃ] ___ ___ ___ ___ [tʃ] ___ [h] [h] [ʃ] ___ [tʃ] ___ [s] ___ ___ [tʃ] [s]}
