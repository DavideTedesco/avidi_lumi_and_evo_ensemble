


quintavoce =
\relative{
    \override Staff.NoteHead.style = #'triangle

%==============C
\stopStaff
  \hide Staff.Clef

  \override Staff.StaffSymbol.line-count = #1
  \startStaff
  \time 5/4	
                                   r1 r4 d4^\mani_\giu^\markup{\italic{apre gli occhi}}  d4_\su d4_\giu d4_\su r4
                                   d4_\giu d4_\su d4_\giu d4_\su  d4_\giu
                                   r2 d4_\su d4_\giu d4_\su
                                   \tuplet 6/5 {d4_\giu d4_\su d4_\giu d4_\su d4_\giu   d4_\su}
                                   \tuplet 6/5 {d4_\giu r4 r4 d4_\su r4 r4}
                                   d8^\mano_destra_\su d8_\giu d8_\su d8_\giu d4^\mani_\su
                                   r2 d4^\mano_destra_\giu
                                   d4^\mano_sinistra_\giu d4_\su d4_\giu
                                   d4^\mano_destra_\giu r4 d4^\mano_sinistra_\su
                                   r2.
                                   r4 d4^\mano_destra_\su r4
                                   d2.^\antiorario
                                   d8^\pugno r4. d4^\orario
  \time 2/4
                                   d2^\orario
                                   r2
                                   r2
                                   r2
                                   \tuplet 3/2 {d4^\orario d4 d4}
                                   r2
                                   d2^\bia
\time 3/4                         
                                   d2.^\bia
                                   r2.
                                   r2.
                                   d2.^\fpa
                                   r2.
\time 4/4
                                   r1
                                   d1^\bia
                                   r1
                                   r2. d4^\pugno
                                   r1
\time 5/4
                                   d4^\pugno d4 d4 r4 d4^\pugno
                                   r2. d2^\biagia
                                   r1 r4
\time 2/4	
                                   
                                   d4^\pugno r4
                                   d2^\biagia
                                   d2^\bia
                                   
                                   r1
                                   d1^\strofinare
                                   d1
                                   d2^\enunciando
                                   r2
                                   d1^\orario


 }
