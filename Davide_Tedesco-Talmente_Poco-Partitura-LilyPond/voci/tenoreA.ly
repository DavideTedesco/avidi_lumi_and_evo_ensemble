

terzavoce =
\relative{
%==============A
\stopStaff
  \override Staff.StaffSymbol.line-count = #1
  \hide Staff.Clef
  \startStaff
  %\diecisec
  %\startGroup
  \alniente
                                     %\A  
                                     \xCircleOnce b1\pp\> % \hideNH    b  b b b                       \uhideNH \uhideStem
                                          (\xNote b4)
                                          \xCircleOnce b1\p % \hideNH    b  b b b                       \uhideNH \uhideStem
                                          (\xNote b4)
                                          %\stopGroup  
                                          %\once \override TextScript.extra-offset = #'( 0 . -1.5 )
                                          %\pausaPos
                                          %\pausaLung
                                          %b 
                                          r4
                                          %\revStemY
                                         %\revStemLung
                                          %\uhideNH 
                                          %\cinquesec
                                          %\startGroup
                                          \xCircleOnce b1\p\< % \hideNH    b  b b b                       \uhideNH \uhideStem
                                          (\xCircleOnce b2\mf)
                                          %\xNote  b\p\< \hideNH   b b b b\mf %\stopGroup 
                                           %\uhideNH \uhideStem

                                         
                                         %\duesec
                                         %\startGroup 
                                         \xCircleOnce b2.\mf %\hideNH   b 
                                         %\stopGroup 
                                         %b4
                                         %\uhideStem \uhideNH
                                         %\cinquesec

                                          %\startGroup 
                                          \xCircleOnce b1 \mf% \hideNH   b b b b 
                                          (\xNote b4)
                                          %\stopGroup
                                         %\uhideStem \uhideNH
                                         %\ottosec
                                         %\startGroup
                                        \once \override TextScript.extra-offset = #'(-4 . 1)
                                         \xCircleOnce b2._\inspirare\p\< % \hideNH   
                                          \once \override TextScript.extra-offset = #'(-4. 0)
                                         (\xCircleOnce b2 %_\espirare
                                         \xNote b4\f)%  b b\f  
                                       
                                         %\stopGroup 
                                         %\pausaPos \pausaLung
                                          r1 r1 r4 r2
                                         %b4 b4 b4 b4 b4 b4 b4 b4 b4\uhideNH \uhideStem
                                                                                    %\once \hide Staff.Clef
                                         %\revStemY \revStemLung



                                           \xNote b4\mf%^\markup{"~1\""} 
                                           %\pausaPos \pausaLung
                                           r2
                                           %\hideNH  b b \uhideNH \uhideStem
                                            %\revStemY \revStemLung                                       
                                           
                                           %\quasec
                                          %\startGroup 
                                          \xCircleOnce b1^\markup{\italic{cresc. fino a}\dynamic{f}} %\hideNH   b b b\f  
                                          %\stopGroup 
                                            %\pausaPos \pausaLung                                                                                                                            
                                          r2%b b
                                            %\revStemY \revStemLung                                       
                                          
                                         %\uhideStem \uhideNH
                                         
                                          \xNote b4\f%^\markup{"~1\""}  
                                                                                     % \pausaPos \pausaLung                                                                                                                            

                                         r2 % \hideNH   b b
                                           %\revStemY \revStemLung                                       
                                           %\uhideNH \uhideStem
                                           \xNote
                                            b4\f\>
                                            \xNote
                                            b 
                                            \xNote
                                            b 
                                            \xNote
                                            b 
                                            \xNote
                                            b 
                                            \xNote
                                            b\mf
                                            
                                            \xCircleOnce
                                            b2.\p^\markup{\italic{cresc. fino a}\dynamic{f}} %\hideNH b b\f 
                                            %\uhideNH
                                            \xNote
                                            b4\mf^\markup{\column{\italic{\line{inspirando ed espirando}\line{ su ogni suddivisione}}}} %\hideNH 
                                            (\xCircleOnce b2.
                                            \xNote
                                            \xCircleOnce
                                            b2\mf\< 
                                            %\hideNH
                                            \xNote
                                            b4\ff )
                                            %\uhideNH
                                            \xCircleOnce
                                            b1% \hideNH b b b
                                            %\uhideNH


            \xCircleOnce  b2\p %\hideNH   b  \uhideNH \uhideStem
            \alniente
               \xCircleOnce  b2.\p\> (\once \hide DynamicText \xNote b4\p)%\hideNH   b  b b  \uhideNH \uhideStem
               %\pausaPos \pausaLung                                                                                                                            

                                          %\hideNH    b b b b
                                           %\revStemY \revStemLung                                       
                                           %\uhideNH \uhideStem
                                           r1
  \stopStaff
                                                \undo \hide Staff.Clef

  \revert Staff.StaffSymbol.line-count
  \startStaff
    r2.
\xNote b4\glissando\f\>^\noiseToSound e2\p^\markup{falsetto} 

\xNote b4\glissando\p\< b2\ff

\xNote b4\glissando\mf bes2

r2.

cis2.\p

ais2.\f

c2 r4

\alniente  f2.\p\> (f2.)\hide DynamicText r2.\p
}


%==============A->B

%==============B
                                    %\B \cinquanta c1

%==============B->C

%==============C
                                    %\C \sessanta c1

%==============C->D

%==============D
                                    %\D r1

%==============D->E


%==============E
                                     %\once \override TextScript.extra-offset = #'(-2 . -1.5)
                                    %\E \settanta d''1_\arrow\glissando e1\f
%==============E->F
                                    %\once \undo \hide Score.BarLine \bar "!"
                                    %\cinquanta  r4 e1 r r a,4 (g) (fis2) (gis) e'1\f \fermata r r

%==============F

%==============TESTO
                                   \addlyrics {[ʃ]  [tʃ]  [h] [ʃ]   
                                               [s] [h] 
                                               [s]  [tʃ]  [ʃ] 
                                               [tʃ] [tʃ] [tʃ] [ʃ] [s] [f] [ʃ] [tʃ] [ʃ] [s] [ʃ] [ʃ]  [ʃ]- [g] [f]- [g] [f]- [m]}
