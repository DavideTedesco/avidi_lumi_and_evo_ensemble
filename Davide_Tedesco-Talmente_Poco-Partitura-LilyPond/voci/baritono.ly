quintavoce = \relative {

%=============F
                                    \bar "!"r1 r1  \alniente 
                                     dis2\p\> dis2\fermata 
                                     d1\mp\<\glissando (
                                     deh \glissando
                                     \once \override TextScript.extra-offset = #'(-1 . 3)
                                     b_\updownarrow\f) 
                                     \breathe                                     
                                     d!1\mp\< (
                                     d)(
                                     \once \override TextScript.extra-offset = #'(-1 . 3.5)
                                     d_\updownarrow\f) 
                                     \breathe
                                     dis1\mp\<\glissando (
                                     fis\glissando 
                                     \once \override TextScript.extra-offset = #'(-1 . 5)                                     
                                     g_\updownarrow\f) 
                                     r1
                                     \breathe
                                     dis\mf\glissando (
                                     \once \override TextScript.extra-offset = #'(-1 . 3)                                                                                   
                                     cis_\updownarrow)
                                     r1
                                     \breathe
                                     b2\pp\<\glissando (
                                     \once \override TextScript.extra-offset = #'(-1 . 3.5)
                                     d_\updownarrow\f) 
                                     \breathe
                                     c2\p\<\glissando (
                                     \once \override TextScript.extra-offset = #'(-1 . 4)                                     
                                     e_\updownarrow\f)
                                    \breathe
                                     r1 \alniente e\mp
                                     \breathe
                                     r2\alniente e4\p\>\glissando 
                                     \once \override TextScript.extra-offset = #'(-2 . 4)                                     
                                     ees_\updownarrow r2 eih
                                     \breathe
                                     g \glissando (
                                     \once \override TextScript.extra-offset = #'(-1 . 4.5)                                                                                                          
                                     fih_\updownarrow)
                                     r1
                                     \breathe                                     
                                     dis1\mp\< (dis\ff)\fermata
                                     r1
                                     \bar  "|."}
%==============TESTO    
                                    \addlyrics {po co  [o] [a] [i] [a] [o] [o] [a] [o] [o] [o] [o] [o]}