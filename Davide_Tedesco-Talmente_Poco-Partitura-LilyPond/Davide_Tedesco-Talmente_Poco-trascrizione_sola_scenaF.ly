%[1]   VERSIONE ==============
\version "2.20.0"

%[2]  BLOCCO PAPER==================================
\include "template/papera4P.ly"

%[3]  BLOCCO HEADER==================================
 \header { 
                 %title = "Talmente Poco"
                 %composer = "Davide Tedesco"
                 %subsubtitle = "per Soprano, Mezzosoprano, due Tenori e Baritono"
                 %tagline = "2022"
 }

%[4]  BLOCCO NOTAZIONE==================================

\include "personalizzazioni.ly"

\include "voci/soprano.ly"
\include "voci/mezzosoprano.ly"
\include "voci/tenore.ly"
\include "voci/tenorebeatbox.ly"
\include "voci/baritono.ly"


%[5]  BLOCCO IMPOSTAZIONE RIGHI==========================
\include "template/Ensemble5Voci.ly"


%[6]  BLOCCO SCORE==================================
       \score {

%[6a]  parti da includere========================

 \new ChoirStaff <<
	\primorigo
	\secondorigo
	\terzorigo
	\quartorigo
	\quintorigo
	>>

%[6b] PERSONALIZZAZIONE GRAFICA==========
 \layout {
               indent = -1

    \context {
      \Score
      \override DynamicText.direction = #UP
      %per spostare le indicazioni dinamiche sopra la partitura
      \override DynamicLineSpanner.direction = #UP
      \override Flag.stencil = #modern-straight-flag
      
      \override Glissando.thickness = #10/3
      % \hide SpanBar 
      %++++++++tweaks for propotional notation
      proportionalNotationDuration = #(ly:make-moment 1/4)
      \override SpacingSpanner.uniform-stretching = ##t
      \override Score.SpacingSpanner.strict-note-spacing = ##t
      \override PaperColumn.used = ##t
      %++++++++tweaks for hiding bars, bar numbers and time
      %\hide BarLine
      %\hide TimeSignature
      \omit BarNumber 
                  \override TimeSignature.font-size = #3


    }
    \context {
      \Voice
      \consists "Horizontal_bracket_engraver"
      \override HorizontalBracket.direction = #UP

    }
    
    \context {
     \Staff 
                  \accidentalStyle Score.modern

    }
  }
%[6c] OUTPUT MIDI===================
%\midi{tempo=60}
} 

%[7] OPTIONS FOR EXPORT===================
%disable hyperlinks
%\pointAndClickOff