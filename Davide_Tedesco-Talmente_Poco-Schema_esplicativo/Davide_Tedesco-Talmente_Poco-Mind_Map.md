---
markmap:
  initialExpandLevel: 0
---

# Talmente Poco


## Voce

### Dal rumore al suono

#### Utilizzo dell'alfabeto fonetico

##### Ricerca di elementi senza intonazione

##### Ricerca di elementi con intonazione

#### Creazione e gestione dello spettro sonoro

#### Utilizzo del fischio

##### Fischio come suono segnale

##### Fischio di gola

##### Fischio normale

### Silenzio

#### Dal silenzio al suono

#### Dal suono al silenzio

### L'interazione di se stessi con l'esterno

#### Interazione uno a uno

#### Interazione uno a molti

#### Interazione molti a molti


----------
## Spazio

### Azione scenica

#### Suddivisione del luogo in aree

##### Caratteristiche sonore e spostamento

#### Spostamento di un soggetto

##### Spostamento di un peso nello schema scenico

### Presenza/Assenza di un individuo

#### Occhi chiusi

#### Occhi aperti

#### Sguardi

### Legame gesto/musica

#### Gesti sincroni

#### Gesti asincroni

#### Sottolineare un elemento solistico

### Legame movimento/prosodia

#### Movimento per mimare una prosodia musicale

#### Movimento come stimolo verso il suono

#### Movimento come unificazione e dispersione di un gruppo

### Grandezza del gesto o del movimento e vicinanza alla voce

#### Gesto e spazio che esso occupa

#### Importanza del luogo in cui si effettua il gesto

#### Quando il gesto diventa movimento?

----------
## Minimalismo delle idee

### Diverso dal minimalismo musicale

#### Essenzialismo

##### Concezione filosofica che afferma la priorità ontologica delle essenze rispetto alle cose nella loro esistenza concreta e individuale

##### Cosa è veramente essenziale?

##### Eliminare ciò che è superfluo

#### Perchè non basta la musica ma serve il testo?

##### Significato

##### Significante

##### Come scegliere il testo?

### Testo breve e con molto significato

#### Varie interpretazioni del testo

#### Frammentazione del testo

#### Fuoriuscita della componente vocalica e consonantica del testo

### Unione/Dispersione

#### Unione/Dispersione dei suoni

#### Unione/Dispersione degli individui

#### Unione/Dispersione delle idee
